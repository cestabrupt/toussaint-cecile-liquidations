---
author: "Cécile Toussaint"
title: "Liquidations"
publisher: "ABRÜPT"
date: "octobre 2018"
description: "L’humanité se liquide sans peine lorsque ses structures s’enflamment. Cette pièce de théâtre se place à une frontière : celle de la parole dite, de la poésie lue, de l’écriture historique de notre futur. Elle parle de l’effondrement, et du sens que l’on cherche désespérément lorsque le spectacle ne miroite plus que le silence."
subject: "Théâtre, liquidation, incendie, départ, émeute"
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0009-8'
rights: "© 2018 ABRUPT, CC BY-NC-SA"
---

# Personnages

\begin{dramatis}
\character[cmd=lingg, drama={Monsieur Lingg}, desc={un homme.}]{Monsieur Lingg}
\character[cmd=oscar, drama={Oscar}, desc={un homme.}]{Oscar}
\character[cmd=michael, drama={Michael}, desc={un homme.}]{Michael}
\character[cmd=samuel, drama={Samuel}, desc={un homme.}]{Samuel}
\end{dramatis}

# Lieux

\begin{dida}
Une pièce. Grande, froide, quelques meubles. Un désert. Comme toute chose. Au loin, des explosions. Le sommeil. Là se tiennent des bibliothèques. Hautes et nombreuses. Des bibliothèques poussiéreuses, emplies de livres poussiéreux. Siècles d’abandon, partout. Au milieu, l’inhabité. Et un fauteuil. Dirigé vers le parterre, vers la ville, vers les coups de feu. Sur le fauteuil, un homme. A les traits creusés. Le rythme lent de la solitude. Jette un regard blanchi en direction des détonations. Il n’a pas d’âge. Une vieillesse incertaine. Il est son regard blanchi. Qui ne regarde rien. Impassible. Regarde devant lui. Rien. La rumeur de ce qui fulmine. Sa confusion avec l’horizon. Il sait. Il est une statue lamentable. À l’égal de tout ce qui sait. Il renonce. Renoncements.
\end{dida}

# Paroles

\begin{dida}
Calme rompu. On frappe. Violemment. La porte parce que le bruit. Trois coups. Silence, à nouveau. L’homme demeure imperturbable. Calme rompu, à nouveau. Trois coups. Brutalité redouble. Et le silence. Porte qui s’entrouvre parce que plus le bruit. Trois hommes. Des pas feutrés. Hors d’eux : langueur. Contraste avec calme rompu, avec porte. Fracas de leur entrée parce que le bruit. Contrastes. Les trois hommes ne sont pas le bruit parce que leur langueur. Et l’écho de leur langueur dans l’homme seul. Mais eux, pas indéterminés. Se meuvent. Encore du côté de la vie. En apparence. Trois hommes, vêtements similaires, gris. Vraisemblablement des uniformes. Le plus vieux des trois remarque la présence du fauteuil. Il remarque l’homme sur celui-ci. Se dirige dans sa direction. Les deux autres, plus jeunes, restent sur le palier. Ils inspectent les lieux. Langueur.
\end{dida}

\lingg[monocorde, regard sur l’horizon] Vous êtes ? \did{Détournant légèrement la tête pour sentir les pas se diriger vers lui.} Vous êtes venus pour la liquidation.

\begin{droite}
Silence.
\end{droite}

\oscar[faisant le tour du fauteuil, se penchant calmement vers \linggname] Nous sommes venus pour la liquidation.

\lingg Soit… Faites donc…

\oscar[signe vers le palier : s’approcher] Vous êtes bien \linggname ?

\lingg[toujours aussi impassible] Oui, je suis bien \linggname. \did{Secouant la tête.} Qui sont les deux autres personnes avec vous ?

\michael[gêné] Nous pensions que vous étiez… \did{Silence.} Comme vous ne répondiez pas…

\lingg Que j’étais ?

\samuel[froid] En allé. \did{Posant une partie de ses affaires.} Peut-être suicidé. Comme les autres. Souvent.

\lingg Je vois. Non, pas encore… \did{Silence.} Je reste la personne que vous cherchez ?

\oscar Oui. Pouvons-nous commencer ?

\lingg[levant la tête vers \oscarname] Oui. Dois-je me retirer ?

\michael[s’approchant du fauteuil, affable] Ne vous dérangez pas ! Nous allons procéder le plus discrètement possible. \did{Geste qui imite le regard de \linggname.} Vous pouvez continuer votre travail.

\begin{dida}
\samuelname et \michaelname sortent de la pièce. Rentrent immédiatement en traînant chacun deux grosses caisses. Les ouvrent. À l’intérieur d’elles, d’autres caisses. Les alignent le long des bibliothèques. Pendant ce temps, \oscarname se tient debout. Au-dessus de \linggname. L’observe sans mot dire. Indifférent. \linggname se réattelle. À sa vue. Son travail. \samuelname et \michaelname à leur travail. Aussi. Se munissent de calepins. Qu’ils commencent à griffonner. Étudient les rayons des bibliothèques.
\end{dida}

\oscar[cessant d’étudier \linggname, des pas autour du fauteuil, lentement] Connaissez-vous la procédure ?

\lingg Oui.

\oscar Je dois vous informer de la procédure.

\lingg Oui.

\begin{droite}
Silence. Réflexions de l’homme qui marche.
\end{droite}

\oscar Nous allons devoir être rigoureux.

\begin{dida}
\oscarname ne cesse de passer devant \linggname. Ne s’en émeut pas. \linggname esquisse un léger mouvement de tête. De côté. Pouvoir continuer son travail.
\end{dida}

\lingg Je ne sais toujours pas qui vous êtes.

\oscar Nous sommes là pour la liquidation.

\lingg[lambin] N’êtes-vous que cela ?

\oscar[l’ignorant] Nous allons agir de la sorte… \did{S’arrêtant. Bureaucratique.} Il est important d’être précis durant toute liquidation. Elles ont toutes un caractère particulier. Une erreur est vite arrivée, et ses conséquences peuvent être fâcheuses. \did{Les mots soulignés. Doctrinalement.} Une grande attention s’avère nécessaire. Votre aide sera précieuse pour le bon déroulement de notre mission. \did{Silence.} Il n’en va pas de sa réussite. Mais bon. C’est une question de courtoisie. Vous comprenez ? \did{Silence. Aucune réponse.} Tout doit être soigneusement planifié afin que notre départ, notre départ à tous se déroule dans les meilleures conditions.

\begin{droite}
Silence. \oscarname continue sa marche.
\end{droite}

\lingg[mouvements de son cou au passage d’\oscarname] Je connais votre procédure. Ils m’en ont déjà informé. Mais, je voulais vous dire…

\oscar[l’interrompant machinalement] tout d’abord… \did{S’arrêtant, se grattant la tête, reprenant son pas.} Tout d’abord, nous allons effectuer le recensement des biens. Selon des critères de type, de taille, de fragilité, d’état de conservation, de couleur, de forme, de fonction subjective, de fonction sociale et de fonction absolue. En cherchant à respecter l’ordre préétabli pour ne pas créer un déséquilibre durant la liquidation. \did{Il indique discrètement du doigt ses collègues, qui continuent d’examiner attentivement les livres.} Comme vous le constatez, il s’agit d’un travail méticuleux d’observation, qui sera suivi d’une série de questions auxquelles vous n’aurez à répondre que par oui ou par non. \did{Stoppant sa ronde.} Vous me suivez ?

\lingg[sans détourner son regard] Je vous suis.

\oscar Ensuite… \did{S’arrêtant, se grattant la tête, reprenant son pas.} Ensuite… Nous passerons à la phase de pondération de l’utilité publique. Chaque objet sera classé en fonction de sa commodité ou de son incommodité en vue du départ. Il sera ainsi attribué à chaque objet un nombre dit d’utilité publique objective, en tant que somme des utilités spécifiques selon les différentes catégories du recensement. Ce nombre sera pondéré par un coefficient issu de la moyenne des nombres d’utilité publique objective de tous les objets recensés depuis ces derniers mois. Bien évidemment. Seuls les objets ayant un nombre d’utilité publique objective pondéré positif seront conservés pour la suite de la procédure. \did{Stoppant sa ronde.} Vous me suivez ?

\lingg Je vous suis.

\oscar Après… \did{S’arrêtant, se grattant la tête, reprenant son pas.} Après… Nous nous étendrons sur l’étiquetage des objets conservés en vue de leur stockage. Cet étiquetage se fera en fonction d’une classification par ordre d’importance d’alpha à epsilon, selon le nombre d’utilité publique objective pondéré, lui-même pondéré par un avis d’utilité subjective que vous émettrez. Vous pourrez faire varier uniquement d’un grade l’ordre établi par les nombres d’utilité publique objective pondérés. Même si ces biens ne sont plus considérés comme les vôtres, mais comme ceux de la communauté en vue du départ, votre avis sera utile afin de les inventorier, puisque ces objets étaient malgré tout en votre possession durant un temps certain. \did{Stoppant sa ronde.} Vous me suivez ?

\lingg Je vous suis.

\oscar Enfin… \did{S’arrêtant, se grattant la tête, reprenant son pas.} Enfin… nous rangerons les objets selon l’étiquetage dans les différentes caisses. Seules les caisses contenant des objets alpha et bêta seront directement envoyées sur la zone de départ, afin qu’ait lieu un nouveau rangement selon des critères d’accessibilité. Pour les autres caisses, une nouvelle procédure de liquidation sera effectuée selon les notes prises aujourd’hui par nos soins ainsi que des critères plus compliqués, dont je vous fais grâce, et qui, je vous l’avoue, ne nous intéresse pas dans notre travail actuel. Un nouveau logement vous sera attribué en attendant le départ. La procédure sera alors terminée, et vous pourrez nous accompagner. \did{Stoppant sa ronde.} Vous me suivez ?

\lingg[fixe] Je ne vous suivrai pas.

\oscar[surpris] Je vous demande pardon ?

\lingg[inflexible] Je ne souhaite pas partir.

\begin{dida}
Silence. \michaelname et \samuelname s’arrêtent. Stupéfaits, un peu. Samuel reprend.
\end{dida}

\oscar[balbutiant] Vous voulez dire… Pas partir avec nous ? \did{Il hésite.} Vous pouvez déposer une réclamation, si vous le souhaitez. Une autre équipe viendra effectuer la liquidation. Nous comprendrions très bien que…

\lingg[inchangé] Non, je veux dire que je ne souhaite pas partir. Du tout.

\begin{droite}
Silence. \michaelname s’approche d’\oscarname.
\end{droite}

\michael[en aparté] Ça n’est jamais arrivé ?

\oscar[recommençant à se gratter la tête] À nous, non… Mais je crois qu’il y a une procédure pour ça…

\begin{droite}
Silence. \samuelname. Son activité. Rythme.
\end{droite}

\michael[à \linggname] Vous avez bien été accepté pour le départ ?

\lingg[observant longuement \michaelname] Oui.

\michael Et vous refusez malgré ça de partir ?

\lingg Je ne souhaite pas partir.

\michael Vous avez ce qu’ils appellent la grande santé ?

\lingg Il me semble.

\michael Je peux ?

\lingg[statufié, un peu, encore] Vous pouvez.

\begin{dida}
\michaelname s’approche de \linggname. Lui saisit le visage. Délicatement. Il observe ses dents. Silence. \oscarname reprend son pas lent. En direction de \samuelname. Il observe les caisses. Observations. Comme s’il cherchait dedans. Une réponse. \samuelname ne se préoccupe pas des autres. Précis. Continue à prendre des notes. Saisit parfois un livre, le repose. Précis.
\end{dida}

\michael[reculant] En effet, vous l’avez… \did{Silence.} Mais cela veut dire quoi… que vous…

\lingg Cela veut simplement dire que je ne souhaite pas partir. Rien de plus.

\michael Vous avez bien été accepté pour le départ ? \did{Silence} C’est une chance pourtant d’être choisi. Beaucoup aimeraient être à votre place. \did{Il montre l’horizon. Cherche le regard de \linggname. Inquiet.} Vous n’entendez pas ?

\begin{droite}
Silence.
\end{droite}

\lingg Je vous entends.

\michael[nerveux] Non pas moi ! Eux ! Ceux que vous observez !

\lingg[dévisageant \michaelname] Je ne vois personne. Je vois la ville. Je vous vois, vous. Et la ville. Et ses flammes.

\michael Mais elle ne brûle pas sans raison, cette ville !

\lingg[négligeant \michaelname] Faut-il une raison ? Réellement ? Ce ne sont que des flammes.

\michael Vous ne voyez pas ? \did{Il tente de dire quelque chose. Il ne le dit pas. Il retourne vers \samuelname. S’assied sur une caisse. \samuelname travaille. L’ignore. Pour lui.} Vous n’entendez pas…

\lingg[sombre, un peu] Je les entends. Comme toute chose qui brûle. Après tout, quelle importance d’entendre… \did{Silence. \michaelname se lève. Il tente de dire quelque chose. Il ne le dit pas. S’assied.} Quelle importance a le papillon de nuit ? Il plonge son existence tout entière dans l’obscurité. Découvre un peu de lumière. Et brise sa réalité. Pour une ampoule luminescente. Pour une fascination. Illusoire. Une courte vie. La fin de cette courte vie, sans doute due à cette fascination. Il ne cesse de rebondir contre les parois incandescentes. Enivré par cette lumière. À sa portée, enfin. Et sa courte vie durant, toute sa courte vie qui lui reste, il s’acharne à tenter. À rater. À tenter quand même. Rejoindre cette lumière. À sa portée, enfin. Et ne jamais la rejoindre. Et rebondir contre le verre brûlant. Oubliant ce qu’il est. Oubliant pourquoi l’obscurité. Pourquoi ce choix de l’obscurité. Pourquoi virevolte-t-il. Pourquoi tant d’adresse. Pourquoi renoncer à la biologie. Et cela, il ne le sait pas. Après tout, ce n’est qu’un papillon de nuit. Le feu, un quelconque miroitement, les bêtes qui se nourrissent d’autres bêtes. La nuit qui vit, et qui vit malgré la biologie. L’humain parmi tout cela. L’humain qui tue ces bêtes, qui tue des bêtes. Sans se nourrir. Qui tue les unes ou les autres. Sans se nourrir. Qui se tue parfois lui-même. Sans se nourrir. À coups d’électricité. Mais malgré : la biologie, le progrès et la biologie, la lumière, en conséquence du progrès ou de l’électricité. Malgré : la satisfaction. Celle des hiérarchies. L’humain socialement repu. Grâce : à son progrès ou à son électricité. Grâce : à lui-même. \did{Silence.} Ne vous y trompez pas ! Je les vois bien. Leur lumière. Celle des flammes. Et ceux qui se précipitent. Les uns contre les autres. Inconscients. Moi parmi eux. Qu’est-ce qu’ils abandonnent ? Je ne sais pas. Du moins, il y a les flammes.

\begin{dida}
\michaelname se lève. Tente de dire quelque chose. Ne le dit pas. Il marche le long des caisses.
\end{dida}

\michael[ailleurs] Que faisiez-vous ? Je veux dire, que faisiez-vous avant la planification du départ ? \did{\linggname ne répond pas.} S’ils vous ont choisi, c’est bien pour quelque chose.

\lingg Je ne sais pas… Peut-être qu’ils font erreur sur la personne…

\oscar[sortant de sa torpeur] Êtes-vous bien \linggname ?

\lingg Oui.

\oscar[dépité, s’asseyant sur une caisse] Il n’y a donc bien aucune erreur.

\michael[réflexions, ailleurs] S’ils vous ont choisi, c’est que vous devez être important pour la suite. Et si c’est le cas, votre choix met en péril le départ, le départ de tous… \did{à \oscarname.} Es-tu sûr qu’il peut refuser ?

\oscar Oui, j’en suis sûr. J’essaie de me rappeler la procédure… C’est que ça ne m’est jamais arrivé. \did{Il s’interroge.} J’ai pourtant souvenir de ce détail durant ma formation… C’est vague.

\lingg[ébloui, un peu] Regardez !

\oscar Quoi ?

\lingg La ville. Qui brûle. \did{Silence. Tous. Regardent.} Il y a toujours quelque chose de fascinant dans une ville qui brûle.

\oscar Oui, ça n’a pas été facile de venir jusqu’ici.

\michael Avec toutes ces émeutes…

\oscar Ils sont sacrément remontés.

\michael Ça va mal finir si ça continue comme ça. \did{Silence. Regards au loin, brefs.} Mieux vaut ne pas y penser. Nous devons continuer, minutieusement. Sans nous attarder.

\lingg[halluciné, un peu] Le feu. Cette cité. La beauté. De la ville ou des flammes, la beauté ? \did{Silence.} C’est beau, n’est-ce pas ?

\begin{dida}
Aucune réponse. Tous regardent. Sauf \samuelname. À son travail.
\end{dida}

\oscar Vous auriez dû voir la colère de ceux qui déclenchent les incendies. Il n’y a rien de très beau là-dedans.

\michael[irrité, un peu] C’est qu’eux aussi, ils mettent en péril le départ. Le départ de tous ! Il y a parfois des choses plus grandes que soi. Il faut l’accepter et se taire.

\oscar Peut-être bien, mais c’est quand même injuste pour eux. Après toute leur attente. Tous leurs espoirs…

\michael[l’interrompant] Il y a parfois des choses… C’est plus grand que soi…

\lingg[halluciné, encore] Voyez ! Cet immeuble au loin ! Comme il s’illumine… Auriez-vous un jour fait attention à cet immeuble sans ces flammes ? La ville devient sa propre lumière. Elle se recroqueville sur elle-même.

\michael Et le processus de départ ? Vous y avez pensé ? Vos flammes… Ça risquerait de tout perturber. Même pire. \did{De l’effroi.} Cela pourrait mettre en péril son sens. La raison commune du départ.

\oscar[de l’effroi, pareil] Et tout recommencerait. Les injustices sociales, la violence, les morts. Comme avant. \did{Silence. De l’effroi, moins.} Ils ont lutté pour que tout ça disparaisse, pour planifier ce départ, cette nouvelle chance. Égale pour tous !

\lingg Voilà pourquoi je ne veux pas partir !

\oscar[surpris] Comment ça ? Vous ne voulez pas y croire ? Les preuves sont pourtant là.

\lingg Je ne veux pas partir, c’est tout. \did{Silence. Faisant signe devant lui.} Pour ne pas manquer le spectacle.

\michael[perturbé] Ce n’est pas un spectacle ! Si nous revenons aux temps anciens… Et si ça recommence… Vous ne vous souvenez pas des horreurs !

\lingg Le spectacle, c’est le brasier lui-même. Au-delà… Il n’y a rien d’autre. Le passé ? Peu d’importance. Il reviendra. Votre avenir ? Même chose. Peu d’importance. Maintenant, là, face à nous, il y a la beauté ! \did{Silence.} Distraits, on s’en détourne, on préfère l’ignorer. Bientôt, plus de beauté. Le feu, disparu. Plus de feu, plus de beauté. Plus de ville. \did{Silence.} Plus que des cendres. Peut-être.

\michael[fatalités, plein] Si nous ne partons pas, voilà ce qui va nous arriver… \did{Silence. Calme, aussi.} Des cendres…

\oscar Je ne pense pas qu’ils laisseront faire bien longtemps. Ou peut-être… \did{Bredouille.} La ville n’est peut-être plus vraiment utile… Comme nous partons…

\michael[à \linggname] Partez avec nous ! \did{\linggname ne répond pas.} Vous devriez partir avec nous. \did{\linggname ne répond pas.} Il n’y aura pas de cendres là-bas.

\lingg Partir. Avec vous. Peut-être. Mais là-bas. Quel là-bas ? Quel endroit ? Quelle est cette destination où l’on peut oublier ? Se fuir soi-même ? Devenir une abstraction ?

\oscar Vous le savez bien. Ils nous l’ont expliqué, en détail. Et ce n’est pas une question de fuite. Mais une question de préservation !

\lingg[distant] Se préserver en négligeant ce qu’il y a autour de soi… \did{Regardant \oscarname.} Votre opération est périlleuse.  Elle porte la destruction en elle. Ce qui se passe ici, elle le reproduira. Là-bas. Ils veulent éviter la confrontation… Et un jour, inévitablement, cela les rattrapera. \did{Silence.} Les mêmes erreurs. Où que nous allions.

\michael[fatalités, moins] C’est pour ce genre de réflexion qu’ils ont dû vous choisir… Ça doit être ça… Vous êtes un homme des questions. \did{Illuminations, un peu. Le quittent. Il observe \linggname. Silence.} Qui êtes-vous pour eux ? \did{\linggname ne répond pas.} Vous devez leur être indispensable.

\lingg[inchangé, inflexible] Je ne vous accompagnerai pas.

\michael[s’accrochant au bras de \linggname, doucement] Je vous en conjure, accompagnez-nous !

\begin{droite}
Silence. Explosions. \samuelname, inchangé, inflexible.
\end{droite}

\lingg Je resterai pour le spectacle. Et après le spectacle, pour les cendres aussi. \did{Silence.} Elles aussi méritent notre attention.

\oscar Vous pensez que vous survivrez seul ?

\lingg[se tournant vers \oscarname] Pourquoi serais-je seul ? \did{Silence.} Que faites-vous de mes souvenirs ?

\oscar[déconcerté] Eh bien… Tout le monde sera parti… Ou sera mort. Alors à quoi bon vos souvenirs…

\begin{droite}
Silence.
\end{droite}

\lingg Je resterai pour l’accalmie. Pour la violence aussi ! Pour la solitude. Pour la mort. Pour le reste. Je cultiverai la destruction et ses fruits. \did{Silence.} Quand le sang et la bile ne peuvent plus être absorbés par la terre, les idées deviennent à nouveau fertiles. Après la violence, il y a l’accalmie, et après l’accalmie, la violence. À nouveau… Mais en attendant la violence suivante, il y aura l’accalmie. \did{Silence.} Ce sera paisible.

\begin{dida}
\michaelname et \oscarname s’approchent de \linggname. Postures de leur corps. Déformations. Vers la conviction, toujours plus de déformations. Tous leurs gestes tentent de le convaincre. Vers la conviction. Leur corps et la peur. Ne pas arriver à le convaincre. La peur. Ils savent qu’ils n’y arriveront pas. Le taisent en eux-mêmes.
\end{dida}

\oscar Cette fois, c’est différent.

\michael La dernière guerre a laissé des cicatrices trop profondes. Plus rien, vous m’entendez, plus rien ne repoussera. Il ne nous reste plus qu’une seule solution pour nous sauver. Le départ sera…

\lingg[lui coupant la parole, avec la même tranquillité] Qu’est-ce qui change cette fois ? En quoi serait-ce différent ?

\michael Cette solution commune ! Qu’elle soit acceptée par tous ! C’est une chance, peut-être la dernière. Un nouveau départ, un nouveau lieu… \did{Il attend une réaction de \linggname. Rien.} C’est un renouveau pour l’humain ! Dans l’égalité !

\lingg[après un temps] Nous reproduirons dans ce nouveau lieu ce que nous sommes ici. Avec la même indisposition à l’égalité. Avec la même violence. Puis la même accalmie. Et les mêmes hiérarchies, la même oppression de l’homme par l’homme. Vos concepts, les miens. L’ampoule, sa lumière. Notre défaite. \did{Silence.} Alors, qu’il est déjà là. Le feu. \did{Tend les bras vers sa ville.} À votre portée. Et vous continuez à l’ignorer. Vous voulez fuir.

\michael Non ! Je vous dis que la situation est nouvelle ! Toute personne a l’opportunité de partir ! Une égalité nouvelle ! Établie avec soin ! Avec leurs compétences ! Ils l’ont dit !

\lingg[vaporeux, vers \michaelname] Votre nouveauté ne semble pas inclure cette ville en flammes… Nos vies ne s’écoulent pas, elles s’en reviennent. Un mensonge, un refus, un autre mensonge. Le recommencement. Notre mort. D’autres après nous. Le recommencement. \did{Bras ouverts, vers la ville.} Qu’est-ce alors que ce feu ? Pourquoi la ville brûle-t-elle ?

\michael Ce ne sont que des événements épisodiques ! Des individus refusant que le groupe soit plus important qu’eux, et… \did{Soupirs.} Ils ont planifié notre départ parfaitement… Et ils prennent toujours les décisions les plus justes. \did{À la ville.} Ces gens-là, qui succombent à… Ils doivent s’en souvenir ! C’est la survie humaine qui est en jeu. Pas seulement la leur, cette fois !

\lingg La survie humaine… \did{Silence. Il sourit à la ville.} Ou celle d’une poignée, de quelques-uns qui ont su jouer de croyances et de… Enfin… N’est-ce pas encore la survie humaine ? Quelques-uns plutôt que le nombre ? \did{Silence.} L’absurde bien aménagé, avec tant de confort. C’est irrémédiable. La ville brûlera complètement. \did{Silence.} Cela n’a pas été facile pour eux, tout cet aménagement, tout cet absurde… Il fallait convaincre…

\michael Nous serons, après notre départ, tous égaux. Tout le monde… Logé à la même enseigne ! Pourquoi ne pas les croire ?

\lingg Je dois avouer… Leur solution est très astucieuse.

\michael[obtus] Oui ! Il le fallait.

\lingg En effet… \did{Silence.} Vous avez tellement de foi…

\michael Oui ! Il le faut.

\lingg Soit… \did{Silence.} C’est certainement pour cela que la ville brûle. \did{Silence. \oscarname et \michaelname aussi le regard, aussi le loin. Seul \samuelname. Impavide. Labeur.} Peut-être que la solution pour votre renouveau, c’est la destruction… \did{\oscarname et \michaelname, fluctuations. Effrayés.} Mais cette fois, totale, sans compromis, sans espoir recouvré après la souffrance. \did{Silence.} Le rien, sa propagation… Et la nature qui comblera le vide… Votre croyance, votre départ… Face aux lois. Inexorables. Celles que l’on tait. Tout se détruit, tout devient autre. \did{Immobile. Un geste vers l’horizon.} Et je préfère observer les flammes. \did{Silence.} Ils se servent de moi depuis bien trop longtemps.

\oscar[étonné] Vous voulez dire que vous avez travaillé pour eux ?

\lingg Ils se servent de moi pour fuir. Pour légitimer…

\oscar[étonné, de plus en plus] Mais de quoi parlez-vous ?

\lingg[dans un murmure] Plus jamais… Nous sommes mensonge… Pas de renouveau… Aucun renouveau pour tous…

\michael[insistant] Que faites-vous pour avoir eu un tel rôle ? \did{\linggname ignore la question. \michaelname s’assied dépité sur une caisse} Vous ne voulez pas me dire… Je comprends… \did{Silence.} Si vous me disiez, je ne comprendrais peut-être pas… Enfin, probablement…

\lingg[d’un ton paisible] Nous sommes mensonge.

\michael[l’ignorant] Ce n’est pas grave… \did{Se relève. Tentative de se convaincre.} Mon rôle participe aussi à la raison commune du départ. \did{Tentative, encore.} C’est une grande chance que j’ai de participer à leur projet ! Je ne comprends pas tout, mais je suis utile. \did{S’assied. Avachi, un peu.} Que souhaiter de plus ?

\lingg Nous sommes mensonge.

\begin{droite}
Silence. \oscarname : les cent pas. Derrière le fauteuil.
\end{droite}

\oscar Je vous comprends.

\begin{dida}
\samuelname arrête sa tâche. Observe. Observations, surtout d’\oscarname. \michaelname vers \oscarname, les bras ballants. Choc en son corps. Contre sa tentative. Se convaincre impossible. Il ne dit rien. Ils ne disent rien. \oscarname marche. Se contente de marcher. Les autres, surtout \michaelname, tentative d’observation. Comme si à force cette parole allait disparaître. \oscarname continue de marcher. Marche. La pièce a la dimension d’\oscarname. \samuelname se fatigue. Plus d’observations. Reprend. Sa régularité. Travail. Sa placidité répond à celle de \linggname. Demeure absorbé par son spectacle.
\end{dida}

\michael Que veux-tu dire ?

\oscar[marchant] Je le comprends.

\michael Pourtant, ton travail… Tout ce zèle…

\oscar Je ne dis rien d’autre… Je le comprends.

\michael[tête baissée] Tu ne peux pas le comprendre, et continuer… \did{Figé par les interrogations.} Non… Comment pourrais-tu continuer… En le comprenant ?

\oscar[ignorant \michaelname, s’adressant à \linggname, continuant sa marche] Restez-vous pour quelque chose en particulier ?

\lingg[se tournant vers \oscarname] Vous voulez dire pour quelqu’un ?

\oscar Oui… Je veux dire pour quelqu’un.

\michael[s’égarant, à lui-même, hésitant, s’égarant encore] Ils ne pensent plus à la mission, à la raison commune, au départ, à ce qui nous dépasse… Pourtant, c’était clair…  L’ordre… \did{À \linggname et \oscarname. Égaré.} Qu’existe… Qu’existe-t-il… Que peut-il exister parmi le désordre ?

\lingg[ignorant \michaelname, à \oscarname] Non.

\oscar Vous voulez dire que vous ne restez pas pour quelque chose ?

\lingg Non, je veux dire que je ne reste pas pour quelqu’un.

\oscar Je vois.

\michael[s’ignorant lui-même, à son tour] C’est ailleurs qu’il faut aller. Il n’y a rien… Ni quelque chose ni quelqu’un… Notre sauvegarde est impossible ici.

\lingg[à lui-même] Non… Je ne reste pas pour quelqu’un.

\michael Rien ne peut être atteint, si l’on ne peut pas l’atteindre ici… Peut-être ailleurs…

\lingg Je reste pour des souvenirs.

\begin{dida}
\oscarname se tourne vers \linggname. Un regard funeste. Pas échangé. \michaelname s’en va. Sur les caisses. S’assied. \samuelname continue. \oscarname, à côté de \linggname. Parallèle. \michaelname : effondrements.
\end{dida}

\oscar Je vous comprends.

\lingg On n’abandonne pas des souvenirs. \did{Silence. Se tournant.} Vous comprenez ?

\oscar Je vous comprends.

\lingg[au loin] Ils risquent sinon de s’évanouir. \did{Geste vers la ville.} C’est comme le feu. Il faut l’entretenir, le nourrir, le protéger. Sinon, la nuit… Vous comprenez ?

\oscar Vous restez pour cette ville ?

\lingg Je reste pour les flammes.

\oscar Je vous comprends. Elles enveloppent vos souvenirs.

\lingg J’aime cette ville.

\oscar[au loin, à son tour] Même parmi les flammes ?

\lingg Surtout parmi les flammes… Elles lui redonnent sa beauté, nous rappellent à son souvenir. Elles révèlent ce que l’on a négligé.

\begin{dida}
\oscarname s’approche de \linggname. Lui saisit l’épaule. Toutes les particules de \linggname tendues vers la ville. Dignes. Inflexibles. Ignorance de ce qui le saisit par l’épaule.
\end{dida}

\oscar[perturbé] Allons-nous tout perdre ? Réellement ? Je veux dire… Si vous avez raison… Allons-nous tout perdre ?

\lingg Nous avons déjà tout perdu.

\oscar Oui… \did{Il s’éloigne.} C’est vrai… \did{Silence.} Mais nous aussi… Allons-nous nous perdre ?

\lingg[inerte, presque] Pourquoi serait-ce différent ?

\begin{dida}
Silence. \oscarname s’assied par terre. À côté du fauteuil. Explosions. Le travail de \samuelname, le reste, rien.
\end{dida}

\oscar Ce n’est pas différent. \did{Silence.} Vous restez donc aussi pour les cendres… Il est vrai que le spectacle après les flammes, on l’ignore le plus souvent.

\lingg[froid] J’aime cette ville… Son incendie… \did{Silence.} C’est peut-être ma faute… \did{Silence.} Je suis son souffle. Avec elle, je dois devenir cendres. \did{Un geste de dépit.} Et à quoi bon résister aux flammes ?

\oscar Je vous comprends. Vous avez déjà tout perdu…

\lingg[se penchant au-dessus d’\oscarname] Vous savez ?

\oscar Je devine.

\lingg Vous aussi ? Vous avez déjà tout perdu ?

\oscar Ils prétendent que le départ prime. La raison commune… Leurs intérêts collectifs… \did{\samuelname s’arrête. Écoute \oscarname. \michaelname demeure interdit. Silence.} C’est quoi l’utile ou le collectif ? Le départ pour tous ? Et comment y penser encore au départ pour tous ? En abandonnant chacun à sa souffrance ? \did{Silence. Il jette un regard à \linggname.} Comment ?

\lingg Regardez ! \did{Silence. Un mouvement harmonieux de la main, vers les flammes.} Nous pouvons admirer. Un dernier spectacle. L’empêcher ? Non. Tout s’achèvera paisiblement. Ensemble. Malgré.

\oscar Qui avez-vous perdu dans ces flammes ? \did{\linggname se tourne vers \oscarname. \samuelname reprend son travail. Silence.} C’était dans les émeutes de ces derniers jours ?

\lingg[monotone] Comment le savez-vous ?

\oscar Je vous l’ai dit… Je devine.

\lingg Non, ce n’est pas cela.

\oscar[étonné] Non ?

\lingg Pas dans les émeutes de ces derniers jours… \did{Silence.} Il y a dix ans.

\oscar Ici ? Dans cette ville ?

\lingg Oui. Quand les attentats ont commencé. Après la guerre. Une fois que nous nous étions habitués à la paix… \did{Silence.} On pensait la douleur disparue, absente de notre territoire… Et pour toujours. Surtout après leurs promesses. Et il y a eu les premiers attentats. Et puis, c’est devenu régulier. Face à cela, ils ont présenté leur solution, le départ. Plutôt que d’y faire face. Plutôt que de comprendre que nous engendrerons toujours la même violence. \did{Silence. \linggname se tourne vers \oscarname.} Toujours, vous comprenez ?

\oscar C’était votre épouse ?

\lingg[l’ignorant] Les responsables des attentats, je ne leur en veux pas. Ils ont tenté d’apporter un renouveau. À leur manière. Pas le même que celui qui anime aujourd’hui notre départ. Mais cela reste un renouveau. À leur manière. Des terroristes ? Peut-être. Ils voulaient imposer… Leur sérénité, leur harmonie. Leur vérité contre les autres vérités. Contre l’ordre établi. Contre ceux qui les considéraient avec morgue. Ils voulaient imposer à leur tour. C’est tout. \did{Silence.} Alors quel meilleur intermédiaire qu’une explosion au détour d’une rue ? Qui fauche au hasard les coupables qui s’ignorent ? Une bombe vaut mieux que des milliers de mots. \did{Silence.} C’est en embrassant le chaos qu’ils ont cherché… Enfin… Qu’ils n’ont rien trouvé de neuf… \did{Silence.} Je ne sais pas. Tout cela doit être aussi un mensonge.

\oscar Vous n’avez pas une photo ?

\lingg Non. J’ai tout jeté. J’ai juste mes souvenirs.

\oscar[en direction de la ville] Et ces flammes aussi.

\lingg Oui.

\oscar Vous voulez la rejoindre ? Par la même violence ?

\lingg Oui.

\oscar La violence est de retour.

\lingg Oui.

\oscar À nouveau, elle semble avoir contaminé la ville. Encore. \did{Il regarde \linggname.} Elle vous a contaminé aussi.

\lingg Elle n’avait jamais disparu.

\oscar Pourtant il y a cinq ans, les responsables des attentats ont été arrêtés ?

\lingg Ils ont été assassinés. Rien de plus. On nous a fait croire que la violence avait disparu avec eux. Qu’on pouvait espérer un changement avec leur idée de départ. Mais la violence a toujours été là. La violence ne s’assassine pas. Elle sommeille.

\oscar Ça ne vous a pas soulagé ? Au moins un peu ?

\lingg Non.

\oscar Leur mort n’a pas été une sorte de rétribution ?

\lingg Non. C’est le hasard le responsable.

\oscar Au moins après leur mort, il n’y a plus eu de bombes.

\lingg Pour un temps… Chez nous… \did{Silence.} Les bombes… On les a exportées. Vendues. Chères. En d’autres endroits. Une autre ville. Quelconque. Où les gens ne peuvent pas réagir. Se taisent. On ne les entend pas les gens quand ils sont ailleurs. Sous les bombes. C’est un soulagement de ne pas entendre les gens. Sous les bombes.

\oscar Cette ville avait au moins retrouvé son calme.

\lingg Ce n’est pourtant pas sa nature, le calme. C’est ennuyeux, le calme. Et le sang ne dilue pas le sang.

\oscar[aigre] Non… Il caille. Rouille. Noircit. Ronge tout ce qu’il tache.

\lingg Je savais que vous comprendriez en tant qu’ancien soldat.

\oscar[étonné, à peine] Vous savez ?

\lingg Je devine.

\oscar Ce sont mes cicatrices ?

\lingg Non, c’est votre démarche.

\oscar Oui, c’est la même chose.

\lingg Cela marque de se battre chez soi. Dans ses rues. Dans le familier. Juste pour espérer. Pour espérer n’importe quoi. Parce qu’il a été dit qu’il fallait espérer. Parce qu’il a été dit que notre sacrifice serait pour la liberté. Se battre dans sa ville. Pour les autres. Oui. Pour la cicatrice.

\begin{droite}
Silence. Des bruits dans le silence. \samuelname au travail.
\end{droite}

\oscar Oui, ça ne vous quitte plus. Une trace indélébile.

\lingg[faisant un geste vers \samuelname et \michaelname] Peut-être ne comprennent-ils pas cela… Tant mieux…

\michael[redressant la tête] Moi aussi… \did{Dans ses pensées, encore.} J’ai fait la guerre.

\lingg[approuvant] Alors vous savez aussi le déchirement…

\michael Oui. Ce truc qui ne disparaît plus jamais. Il faut composer avec.

\oscar Ces derniers jours, avec leur annonce, avec les explosions qui recommencent, je pensais retrouver les mêmes attitudes, le même mode opératoire. Mais non…

\michael Les gens ne se cachent plus.

\lingg Si vous le dites.

\oscar Vous n’êtes pas d’accord ? C’est pourtant bien différent ! Ils avancent le visage découvert.

\lingg Je ne sors plus trop de chez moi. Cette vue me suffit. Je contemple avec distance.

\oscar Eh bien, ils ne craignent plus les répercussions. Ils sont comme désespérés.

\michael Ce n’est pas politique cette fois. Juste une question de survie.

\lingg Pourquoi juste une question de survie ?

\oscar Vous ne lisez pas la presse ?

\lingg Non, cela ne m’intéresse plus vraiment…

\michael Ces derniers jours, ils ont durci les conditions de départ. Ils ont instauré des quotas supplémentaires.

\oscar Et ce que vous contemplez avec distance, c’est de la frustration.

\lingg Vous pouvez appeler cela comme vous voudrez… \did{Silence.} Moi, je ne vois que des flammes.

\oscar Vous avez renoncé, vous ne vous rendez pas compte… Mais ces gens ont tout perdu. Ils espéraient partir, et puis…

\michael[l’interrompant, morne] La mort.

\lingg Le feu n’apporte pas la mort.

\oscar Oh, elle viendra suffisamment tôt. Plus rien ne pousse.

\michael Je vous le dis, ce sera la mort !

\lingg Le feu n’apporte pas la mort. \did{Silence.} Il fertilise la terre. Cette terre que vous voulez abandonner. Où rien ne pousse.

\oscar Imaginez ! Tout cet espoir qu’ils ont placé dans une vie nouvelle.

\michael Et du jour au lendemain, plus rien.

\lingg Peut-être l’ont-ils mal placé ? Pourquoi confier son espoir à des promesses ? Attendre des effets différents. Et la déception. Identique.

\michael Ils ont durci les conditions de départ pour une bonne raison, pour sauvegarder une raison commune.

\lingg Qui profitera à qui ?

\oscar[perdu] À notre survie, à notre mémoire peut-être ? \did{Silence.} Sinon quel serait le sens de ces milliers d’années ? De notre histoire ? De nos souffrances ?

\lingg Pourquoi devrait-il y avoir un sens ? Les étoiles naissent et disparaissent. D’un fracas à l’autre. L’espace s’en nourrit, s’étend. Et nous… Nous sommes simplement égarés au milieu de ce tumulte.

\oscar Je ne peux pas l’accepter.

\lingg[compatissant] Vous avez peur de ne pas partir ?

\oscar Peut-être… Que me resterait-il ?

\lingg Comme moi… \did{Silence.} Vous avez tout perdu, et vous décidez d’abandonner le peu qu’il vous reste.

\oscar Une volonté de survivre. Simplement.

\lingg Il est difficile de survivre en abandonnant.

\oscar[résigné] Je n’abandonne rien. Je suis différent de vous. Il ne me reste plus rien. Même plus de souvenirs. \did{Silence.} Je hais cette ville.

\lingg Qui avez-vous perdu ?

\oscar[l’ignorant] Je me souviens de la procédure en cas de refus du départ. Voulez-vous que je vous la signifie ?

\lingg Je la connais déjà.

\oscar[administratif] Nous procéderons ainsi… Je tiens à vous signaler que la procédure est nettement simplifiée du fait de votre refus, car nous soustrayons à la procédure précédemment énoncée toutes les parties concernant vos interventions…

\michael[surpris] Il n’a plus son mot à dire ?

\oscar Non… Pour autant que je sache…

\michael Mais ce sont ses biens !

\lingg Ne vous en faites pas ! Je ne suis pas attaché à ces choses. Tant que je reste ici.

\michael C’est injuste !

\lingg J’étais déjà au courant de cela.

\oscar[continuant] Nous nous substituerons donc à votre avis pour apporter la subjectivité nécessaire à une liquidation la plus utile possible pour la raison commune.

\lingg Comme je vous l’ai dit, je suis au courant.

\oscar Vous auriez pu profiter de ces biens, et de ceux mis en commun, ceux des autres voyageurs, en décidant de partir…

\michael[lui coupant la parole] Je te dis que c’est injuste. On ne peut pas faire ça ! Pourquoi ont-ils prévu de telles règles ?

\oscar Je ne sais pas. C’est comme ça.

\michael Mais ça va arriver partout ? Même chez les personnes qui ne peuvent pas partir ? Lorsque toutes les liquidations des voyageurs auront été effectuées… On va devoir prendre les biens de ceux qui restent ?

\oscar[impassible] Il me semble… En fait, seulement s’il manque des biens utiles au départ, et que ces biens ne peuvent pas être fournis par l’administration.

\michael On va donc voler les gens qui restent ici !

\oscar Non, liquider leurs biens… Tu oublies l’utilité publique, la raison commune, le départ…

\michael[honteux] Et ceux qui ne partent pas ? Et lui, il refuse par exemple…

\oscar Ce sont les règles… Je n’y peux rien.

\michael Donc c’est une décision punitive, pour le forcer à partir ! On lui laisse seulement l’inutile s’il continue à refuser…

\oscar[gêné, s’adressant à \linggname] Il ne vous restera en effet que les biens inutiles au départ. Et les biens qui seront jugés inutiles durant les tris suivants ne vous seront pas rendus. \did{Silence. \michaelname se détournant. Se renfermant en lui-même} Je suis navré, ce n’est pas moi…

\lingg[levant la main pour le faire cesser] Je vous ai dit. Je suis au courant.

\michael[se ravivant] Peut-être pourriez-vous faire un recours ? Dénoncer ! Parler ! N’importe quoi ! Vous devez avoir de l’influence ! Quelqu’un vous entendra peut-être.

\lingg Rejoindre les protestations ? Jeter le feu sur la ville… Peut-être… \did{Silence.} Je crois que je préfère rester là, contempler la ville qui brûle…

\oscar[vide] Avez-vous compris cette procédure ? Je dois avoir confirmation que vous avez bien compris la procédure en cas de refus. Vous pouvez encore revenir sur votre décision, si vous le souhaitez.

\michael[à \oscarname] Et nous ? On ne peut rien faire contre ça ? Je n’ai pas signé pour ça !

\oscar Non, on ne peut rien faire… On doit obéir, ou alors…

\michael Ou alors quoi ?

\oscar Il nous arrivera la même chose. Sans doute. Si l’on parle…

\lingg Nous sommes un mensonge.

\oscar[ignorant \linggname, questionnant \linggname] Avez-vous compris ?

\lingg Comme je vous l’ai déjà dit…

\oscar[hésitant] C’est donc oui ? \did{Silence.} Vous êtes sûr ?

\lingg Oui, oui. Procédez !

\oscar Comme vous voudrez… \did{A \michaelname. Montrant \samuelname} Viens ! Faisons comme lui ! Ce sera moins… Ça passera plus vite.

\begin{dida}
Direction : \samuelname. Lui continue. Son travail, hors du monde. Comme si. Plus rien autour de lui. En dehors. \oscarname et \michaelname, imitations. Se risquer aussi à être en dehors. Pas la même dextérité. Ils saisissent des livres de façon hasardeuse. Les ouvrent, prennent des notes. Puis les reposent. Travail machinal, ennuyeux. Seul \samuelname semble s’extraire. Sa tâche, un geste parfait. En dehors. Après un temps. \michaelname, un livre à la main. Se dirige vers \linggname.
\end{dida}

\michael Vous ne voulez pas me répondre ?

\lingg Je veux bien.

\michael[confus] Je veux dire… Vous ne m’avez pas répondu… Que faites-vous dans la vie ? \did{Silence. \linggname l’ignore.} Pourquoi autant de livres ?

\lingg C’est pour mon travail. \did{Silence.} C’était pour mon travail.

\michael Vous avez… Je veux dire… Vous aviez besoin des versions papier pour votre travail ? Aujourd’hui pourtant… Tout est dématérialisé. C’est plus pratique.

\lingg Je sais. C’est ennuyeux.

\michael Et votre travail avait une telle importance pour eux ? Pour le départ ? \did{Il montre le livre.} Avec des livres… Je veux dire… Du papier.

\lingg Il faut croire.

\michael Vous deviez être une sorte de… chercheur… Qui cherche dans les vieux livres… Des choses que leur technologie ne dit pas…

\oscar[en continuant son travail] Ils prétendent pourtant qu’elle dit tout.

\michael[à \oscarname] Ça ne doit pas être le cas, sinon nous ne serions pas là.

\lingg[éteint] Et vous oubliez que le papier brûle mieux.

\michael[inquiet] Si le feu se propage, d’ailleurs, cela risque de devenir dangereux pour vous. Vraiment, vous savez, tout ça… C’est très inflammable ! Vous devriez partir. \did{Silence. \michaelname observe le livre.} Qui êtes-vous \linggname ?

\lingg Je suis \linggname.

\michael[gentiment] \linggname, je le sais ça… Mais vous n’êtes pas seulement \linggname. Qu’êtes-vous d’autre \linggname ?

\lingg[incertain] Je ne suis rien d’autre.

\michael Et que disent tous ces livres ?

\lingg Ils disent des vérités.

\michael[soucieux] Il n’y en a pas qu’une seule de vérité ? C’est ce qu’ils nous racontent pourtant.

\lingg Ils le font croire, oui… Ils affirment qu’ils disent la vérité. Les responsables des attentats aussi. Ceux qui brûlent la ville sans doute également. Tous se prétendent supérieurs. Détenteurs de la vérité. Ils s’entre-déchirent. \did{Silence.} Et vous, vous travaillez. Pour eux. En silence.

\michael Enfin, ils ont été élus ! Nous les avons choisis.

\lingg Oui… Que voulez-vous… Il faut bien quelques hiérarchies, quelques choix, quelques cadavres pour attiser la vérité. Être légitime, puis détruire, absorber les puissances anciennes, devenir plus puissant, se passer de la légitimité…

\begin{dida}
\samuelname continue. Laborieux. Sa mission. Tous sont du côté du silence. Pas lui. Tous au silence. Silence.
\end{dida}

\lingg[reprenant] Oui… C’est ainsi. Alors, ce papier peut bien être raturé de mots… À la fin, cela ne changera rien.

\michael Vous y avez cru aux vérités de ces livres, n’est-ce pas ? \did{\linggname ne répond pas.} Au moins, un temps…

\lingg Oui.

\michael Et c’était agréable ?

\lingg Non, pas plus qu’autre chose. Le temps passe plus vite, c’est tout.

\michael Mais vous avez continué ?

\lingg Oui… Les livres sont tenaces. Ils s’accrochent à vous, et ne vous lâchent plus. Eux aussi, ils vous font croire leurs vérités. Avec violence. Ils les imposent. Alors, vous suivez.

\michael Mais… Que disent-ils précisément ? \did{Silence.} Pour qu’un homme comme vous puisse suivre… \did{À tâtons.} Du papier… Malgré sa violence…

\lingg Oh, ce qu’ils disent… \did{Silence.} Le plus souvent, ils le disent à côté du monde.

\michael[triste] Et vous avez renoncé à les comprendre ?

\lingg Je les ai compris. Et puis, j’ai renoncé.

\michael Vous voulez dire…

\lingg Vous pouvez tous les prendre.

\michael Vous voulez dire…

\oscar Je crois malheureusement qu’ils ne seront pas d’une grande utilité.

\michael[curieux, s’approchant d’\oscarname] Pourquoi ça ? Certains d’entre eux sont très anciens.

\oscar Je sais… C’est navrant, mais toute l’information de ces livres est déjà répertoriée. \did{\oscarname montre les rayons.} Ce qui compte pour le départ, c’est l’information elle-même. Pas les livres.

\michael L’objet ne compte pas pour eux ?

\lingg Bien évidemment que non. Tout comme nous… Une fois que nous ne sommes plus utiles.

\michael Comment ça plus utiles ?

\lingg Je suis désolé.

\oscar[d’une voix apaisante] Ce qui compte c’est l’acuité de ton travail, après… Cela ne nous regarde pas… La même acuité que l’information recueillie dans ces livres. Tu vois, une fois enregistrée, plus besoin de revenir dessus. Le travail est fait, bien fait !

\michael[faiblement] Bon, pas de livres alors… On se contente ici des produits électroniques, c’est ça ? \did{Regarde autour de lui.} Il n’y aura pas grand-chose à prendre…

\oscar Non… On liquide le nécessaire… \did{S’approchant de \michaelname. Objet à la main. Une quelconque technologie. Pas un livre.} Tu vois, ce qui compte avec ces circuits électroniques, ce n’est pas l’objet en tant que tel. Ça, on s’en fiche ! C’est la matière première. L’objet sera détruit, on recyclera ce qui peut être recyclé. C’est ça la vraie ressource de notre époque ! Surtout que maintenant… Plus rien ne pousse… Comme ça, on pourra produire d’autres circuits électroniques. Ça facilitera notre départ. Davantage de données pourront circuler…

\lingg Et vos données tournoieront… Merveilleuses… Sans vous…

\michael Alors que dans le livre…

\oscar Il n’y a rien à recycler !

\michael[acceptant] Il y a juste de l’information… Déjà collectée.

\lingg Et de l’encre ! Et du papier ! Et du papier qui brûle !

\oscar[studieux] C’est bien ça le souci. On ne peut pas se permettre un tel risque.

\lingg Vous fuyez les flammes, mais elles vous rattraperont.

\michael[montrant la pièce, à lui-même] Donc ici, on ne prendra quasiment rien… \did{Silence.} Tout semble si vieux.

\oscar Oui, j’en ai bien peur… C’est malheureux, mais ce sont les règles. Ne pas s’occuper de l’inutile.

\michael[transparent] Et ici, tout est inutile.

\lingg[moqueur, un peu] Oui, et je suis bien à ma place. \did{Pointant la ville. D’une voix fascinée.} Regardez comme tout ceci est inutile !

\michael Vous voulez être inutile, \linggname ! Ils veulent que vous partiez avec eux. Il y a une raison à cela.

\oscar[s’approchant] Tu dois respecter son choix maintenant.

\lingg Vous pouvez rester si vous le souhaitez ! Pourquoi vouloir vous sauver ? Il n’y a rien de beau dans ce qui est utile. Une fois parti, vous ne verrez jamais d’aussi beau spectacle. Aussi inutile.

\oscar[sévère] Tu n’es pas obligé de l’écouter ! \did{Saisissant \michaelname. L’attirant vers le travail. Son travail.} Travaille ! Sauve-toi !

\begin{dida}
\samuelname cesse. Plus à sa tâche. Direction : vers le groupe. Tous sont surpris. \linggname se détourne. Plus de spectacle. Étonnement empêche. Il observe \samuelname. Qui se tient à ses côtés. Plongé dans un livre. \samuelname ne regarde pas. Il referme le livre. Il regarde. Tient solidement de ses deux mains le livre. Regarde intensément le livre. Puis \linggname. Silence. Il lui tend l’ouvrage. \linggname le saisit. Le regarde, regarde \samuelname. Le feuillette. Interrogateur. Tente de comprendre. L’attrait de cet objet. En particulier. Il regarde \samuelname. Le livre. \michaelname et \oscarname sont pétrifiés. Leurs corps qui demandent pourquoi. \linggname aussi. Pourquoi. Cette irrégularité. Plus à sa tâche. Tout demande pourquoi. Pourquoi cesser d’agir, de travailler. De réguler l’environnement. Mécaniquement. Plus la rigueur du geste. Sa justesse. Pourquoi. Rien ne parle. Silence.
\end{dida}

\lingg[hésitations] Oui ?

\samuel[grave] Pourquoi ?

\lingg Eh bien… \did{\linggname observe encore une fois le livre.} Je ne sais pas…

\samuel Pourquoi ?

\lingg Qu’a-t-il de différent ?

\samuel Il est interdit.

\oscar[apeuré, un peu, vers \samuelname] À la vente ! Il n’y a pas d’interdiction à le posséder.

\samuel[à \linggname] Il est interdit. Comment l’avez-vous obtenu ?

\michael[apeuré aussi, un peu] Tu vas lui causer des problèmes. Ça n’a pas d’importance.

\oscar Son contenu est bien répertorié dans les bases de données ?

\samuel[en continuant à fixer \linggname] Oui !

\oscar Ça n’a donc pas d’importance ! Ne sois pas aussi borné ! Il a raison, tu vas lui causer des problèmes !

\samuel[ne cessant de dévisager \linggname] Il est interdit ! Ce livre est subversif !

\oscar Ce n’est pas grave. En plus, il ne part pas…

\lingg[reprenant sa posture indolente] Peut-être. \did{Silence.} Certainement. \did{Silence.} Cela dépend pour qui. \did{Silence.} Cela dépend de la vérité que vous voulez imposer.

\samuel L’auteur est mort. Il a été jugé.

\lingg Jugé, condamné, exécuté, assassiné, brûlé, interdit… Ce ne sont que des mots… Il n’est plus là.

\samuel Comment l’avez-vous obtenu ? Il n’y en a quasiment plus. La plupart ont été détruits.

\lingg Je connaissais l’auteur. \did{Silence.} Vous savez, il n’y était pour rien dans les attentats. Il voulait de la justice, lui aussi, une justice équivalente de part en part de la société. De l’égalité. Aussi. \did{Il tend le bras.} Partout dans la ville, jusque dans ses bas-fonds. Ces zones que l’on veut aujourd’hui fuir. \did{Silence.} Il n’y était pour rien. \did{Silence.} Et maintenant son souvenir qui oscille parmi ces flammes. \did{Silence. \linggname se tourne vers \samuelname. Il lui tend le livre. \samuelname le récupère.} Je vous le donne si vous le souhaitez.

\samuel Je possède déjà un exemplaire.

\begin{dida}
Silence. Tous scrutent \samuelname. L’interrogent. Ses réactions.
\end{dida}

\lingg Vous connaissiez l’auteur ?

\samuel Je connais ses écrits. N’est-ce pas la même chose ? N’est-ce pas ça, vos souvenirs ?

\lingg Vous… \did{Silence.} Vous comprenez.

\begin{dida}
\samuelname ouvre le livre. Paisible. Paisible ou grave. Le réel s’érode. Il lit en silence. Silence.
\end{dida}

\samuel[puis à haute voix] Je m’arrache le cœur sans crier gare. Faux-monnayeur, je ne demande rien en retour. Il vient d’une main tendue qui ne se refuse pas. Je le dépose en offrande. Je parie cent sous contre un franc. Il sera précieux. Bouillonnant, il gît là. Au-devant des autres. Et au-devant de lui, les autres. Mais les autres se ressemblent tous à présent. Ils sont devenus l’ombre, mon ombre. J’en fais mon obole. Personne ne vient. C’est trois fois rien, et pourtant c’est déjà beaucoup. Il verse à boire. Intarissable, il étanchera la soif. Il y aura des réjouissances. Partout la fête. Je n’en ai pas besoin, si les autres sont dans le besoin. Mais les autres se ressemblent tous à présent. Ils sont devenus l’ombre, mon ombre. Je le laisse. Et il demeure là. Régulier. Irréductible. Il bat la mesure. Pour que les autres s’y abreuvent et pour qu’ils y reviennent. Toujours, et toujours exempts de toutes craintes. Mais les autres se ressemblent tous à présent. Ils sont devenus l’ombre, mon ombre. Mon cœur faiblit. Il sonne fêlé. Je crie pour avertir. Personne ne m’entend. Une ombre ne s’occupe de rien, si ce n’est de son écho. Elle attend péniblement les cahots, le chemin. Elle n’observe pas les lueurs, le lointain. Elle bat le rappel après l’orage. Elle se détourne de ce qui éblouit. Elle se contente de voir midi à sa porte. Et porte reste close. Malgré les plaintes, malgré les rires. Elle retentit. On frappe. Mais reste close. Close aux choses, aux avertissements, aux joies, aux simples, à la peine des simples, à l’aurore, à l’oubli. Aux cris. Aux cris du cœur. Désolé. Il ne frappe plus. Cœur désolé. Cœur sans. S’en est allé.

\michael[attentif] Et après ?

\begin{dida}
\samuelname referme le livre. Brutalement, un peu. S’en retourne. Vers les bibliothèques. Le livre dans son rayon. Reprend son travail. Tous l’observent. Cois. Après un temps, \oscarname sort de sa poche : paquet de cigarettes. Une, s’allume. Lentement. \michaelname sidéré, un peu. Réel qui chavire. Autour de lui, mais encore du réel.
\end{dida}

\michael Mais…

\oscar Oui, j’ai repris.

\michael On n’en trouve plus pourtant !

\oscar[fumant] Il y a encore le marché noir.

\lingg[à \oscarname] Vous aussi.

\oscar Moi aussi.

\michael Mais… C’est interdit… \did{\oscarname en offre une à \linggname. Feu. Fument.} Les candidats au départ doivent respecter ça… \did{Silence. Fument.} Pour des questions de santé publique !

\lingg[à \michaelname] Il n’est plus candidat.

\michael Comment ça, il n’est plus candidat ?

\lingg Il ne part pas.

\michael Comment ça, il ne part pas ?

\oscar Quelle importance…

\michael Tu as décidé ça maintenant ? À cause du livre ?

\oscar Non… Il y a un moment. \did{Silence.} Je n’ai pas eu le choix.

\lingg[médusé, à la ville] Nous sommes son mensonge.

\oscar Tes questions de santé publique… \did{Bouffée sur cigarette.} Elles ont eu raison de moi.

\michael[interloqué] Pourquoi tu me parles de santé publique ? Tu es malade ?

\oscar Non… Enfin, je ne crois pas. \did{Silence. Se tourne vers \michaelname.} C’est toi qui m’en parles de santé publique. Je ne sais même pas ce que ça veut dire une santé publique. Il faut croire que je ne fais plus partie de leur public ou de leur santé. Ou peut-être des deux.

\michael Ils t’ont contacté ?

\oscar Oui. Ils m’ont rejeté. C’était bref.

\begin{droite}
Silence. \samuelname s’arrête. Observe \oscarname.
\end{droite}

\michael Rejeté ?

\oscar Oui. Rejeté du programme. Pas de départ pour moi.

\michael Ils t’ont donné une raison ?

\lingg Existe-t-il une raison pour tout ?

\oscar Oui, il y a une raison. Santé trop fragile.

\begin{droite}
Silence. \samuelname se remet. Sa tâche.
\end{droite}

\michael Ce n’est pas possible. Tu te portes comme un charme. Et tu viens de dire…

\oscar[affligé] Je sais. Je ne comprends pas moi-même. Mes derniers examens n’indiquaient rien. La dernière fois que j’ai vu le contrôleur médical, il m’a dit que j’avais la grande santé.

\michael Et tu ne te plains jamais… Malgré ton âge.

\oscar Je ne suis pas si vieux… Non ?

\michael[menteur] Bien sûr que non ! Et en pleine forme, regarde-toi ! \did{Il s’approche de lui.} Je peux ?

\oscar Tu peux.

\begin{dida}
\michaelname tire doucement les joues d’\oscarname. Ses dents. Examiner. Il examine. Sous plusieurs angles. Il recule.
\end{dida}

\michael[dépit] Tout indique que tu as bien la grande santé ! \did{Il se tient la tête. La lâche.} Je ne comprends pas.

\oscar Moi non plus. Je me réjouissais de partir avec vous. C’est qu’il ne me reste rien ici.

\lingg Nous sommes mensonges.

\oscar[à \linggname] Vous pensez que c’est à cause de mon âge ?

\lingg Nous sommes mensonges.

\oscar Je ne mens pas sur mon âge. Vous me trouvez vieux ?

\lingg[menteur] Non… Vous devez être plus vieux que moi, et néanmoins je suis plus vieux que vous.

\oscar[préoccupé] C’est bien ce qu’il me semblait. C’est à cause de mon âge. \did{Silence. Menteur.} Je ne comprends pas. Je ne suis pourtant pas si vieux.

\lingg Il n’y a rien à comprendre. Il n’y a pas de raison. Nous sommes mensonges.

\michael Et ça fait longtemps que tu es au courant ? \did{Il montre la ville.} Ou c’est comme les autres ?

\oscar Ça fait un moment.

\michael[surpris] Tu travailles pourtant comme un acharné… J’ai pas souvenir… D’un changement dans ton comportement !

\oscar Non… Rien n’a changé. J’ai continué.

\lingg[à lui-même] Rien ne change.

\michael Tu continues à travailler pour eux malgré ça ? Tu n’es pas… J’en sais rien… En colère ?

\oscar Ils doivent avoir leur raison.

\michael[dégoûté] Quelle raison ?

\oscar Je ne sais pas… Et je me suis habitué à ce travail. \did{Silence.} Je ne sais rien faire d’autre.

\michael Pourquoi ne pas nous l’avoir dit plutôt ?

\oscar Je ne voulais pas vous perturber. Surtout toi !

\michael Pourquoi surtout moi ?

\oscar Tu sembles de plus en plus tendu à mesure que le départ approche. Particulièrement depuis que les émeutes ont commencé. Quelque chose te préoccupe. Je ne voulais pas t’inquiéter davantage. Et surtout, je ne voulais pas de votre pitié.

\michael Ça ne change rien… Ça veut dire que tu ne veux pas faire recours ?

\oscar À quoi bon ? Tu as déjà entendu parler d’un recours qui a abouti ?

\michael Non… Je ne crois pas… Mais tant qu’on peut faire recours… \did{Silence.} Il y a une chance, non ?

\oscar[regardant sa cigarette] Je fumais autrefois… J’aimais bien ça. Et puis, ils l’ont interdit après la guerre. \did{Silence.} Et j’ai arrêté, comme la plupart des gens.

\michael Tu fumais beaucoup ?

\oscar Non… Je ne crois pas… \did{Silence.} Tout le monde fumait avant.

\lingg[étouffant, un rire] Et maintenant, la ville fume… Et nous reprenons avec elle…

\begin{dida}
\oscarname regarde en direction de la ville. Sa cigarette. La ville, encore. Sourit. Sourire triste, un peu. Un dernier nuage de fumée. Semble être ce nuage. Cigarette écrasée. Du bout du pied. Se plante face à la vue. Le regard en écho à celui de \linggname. \michaelname titube. Jusqu’aux caisses. Prostré à côté de celles-ci, à même le sol. Assis. La raison qu’il cherche tant. Renonce. Il semble. \samuelname s’arrête. L’examine. Fin de l’examen. Reprise. Indifféremment.
\end{dida}

\oscar Ils pourraient au moins la remettre sur le marché. Légalement. C’est que ça tient un peu compagnie… Que tout le monde puisse se remettre à fumer librement… Qu’on nous foute la paix… \did{Silence. Il se tourne vers \michaelname.} Tu semblais si heureux qu’on parte ensemble, tu parlais de nos vies qui seraient différentes, meilleures… Je suis navré… Tu ne cessais pas de nous en parler… Alors, j’ai préféré me taire. Je ne voulais pas… Je ne voulais pas gâcher ton enthousiasme. \did{Silence. \linggname regarde sa cigarette s’éteindre. La jette.} Mais tu verras… Avec ou sans moi… Cela ne changera rien. Tu trouveras ce que tu cherches.

\lingg Il ne sait pas ce qu’il cherche. Pourquoi le trouverait-il là-bas ? Il ne voulait pas se retrouver seul. Il est comme tous ces gens qui allument des feux. Ils ne savent pas pourquoi.

\oscar Oui… \did{Silence. Sort de ses pensées. Va vers \samuelname.} Il faut qu’on se remette au travail. On a encore à faire.

\begin{dida}
\oscarname comme \samuelname. Au travail. Explosions. \michaelname reste avachi.
\end{dida}

\lingg Pourquoi continuer ? Votre collègue a raison ! \did{Silence. À \oscarname.} Cela n’a plus vraiment de sens maintenant. Ils vous abandonnent. À votre tour, abandonnez !

\oscar Je ne sais rien faire d’autre.

\lingg Une question d’habitude… Choisissez une autre habitude. \did{Mordant, un peu.} Votre travail vaut certainement mieux que d’attendre comme moi, n’est-ce pas ?

\oscar[continuant son activité] Je ne sais pas comment vous faites. Je deviendrais fou à votre place.

\lingg Peut-être l’êtes-vous déjà ? \did{Silence.} Peut-être le suis-je aussi d’ailleurs… Quelle importance puisque nous restons ici…

\oscar Il n’y a rien de plus terrible que d’attendre… C’est une torture. Je préfère travailler pour ne pas penser.

\lingg Parce que vous ne pouvez plus fuir ? C’est cela ? \did{Silence.} Vous voulez fuir ?

\oscar En attendant… \did{S’arrête. Silence.} On se laisse submerger. Il y a les questions… \did{Silence. Reprend.} Mieux vaut travailler… Au moins, les questions nous laissent en paix.

\lingg En somme, vous êtes obéissant à ceux qui vous refusent une vie meilleure ?

\oscar[indifférent, un peu] Sans doute.

\lingg[regardant \oscarname] Vous avez l’air obéissant, oui… Et pas du genre à vous plaindre. Je me trompe ?

\michael[tête baissée] Il ne se plaint jamais.

\oscar[indifférent, un peu plus] En effet, je n’aime pas me plaindre.

\lingg Vous ne contestez jamais les choix de votre hiérarchie…

\oscar Enfin… Je ne crois pas.

\lingg La chose est entendue. Vous êtes une bonne bête.

\oscar[satisfait ou agacé, satisfait] Oui, pourquoi pas… Je fais mon travail, et je le fais bien.

\lingg Vous avez à n’en point douter plus de responsabilités que vos collègues…

\michael En fait, c’est lui qui dirige…

\lingg Et vous êtes fier de cette responsabilité. Encore aujourd’hui ! Malgré leur décision.

\oscar Eh bien, j’estime de ma responsabilité la qualité du quotidien de mes collègues.

\lingg[amusé] C’est ce qui a dû les séduire…

\oscar Peut-être bien !

\lingg Et aujourd’hui, c’est la raison pour laquelle… \did{Se tourne. Regarde \oscarname.} Il vous liquide. Vous continuerez à ne pas vous plaindre.

\oscar[mélancolique, un peu] Même si je voulais… Qu’est-ce que ça changerait ? \did{Silence.} Et je ne sais pas faire. Me plaindre. Je ne sais pas.

\lingg Vous n’êtes pas maître de votre destin. \did{Silence.} Nous sommes de bonnes bêtes. Ils savent que nous avons renoncé à nous débattre. Et ils en profitent ! Pourtant. Nous avons des armes ! \did{Silence.} Oui, nous sommes de bonnes bêtes.

\michael[s’effaçant, peu à peu] Au fond, ils ont bien raison ! Ils travaillent eux aussi. Pour quelque chose de plus grand…

\lingg[enjoué, mais grisâtre, toujours] Cette capacité machinale à suivre les ordres… Quelle extraordinaire habitude ! Depuis si longtemps, on nous explique comment vivre et comment mourir. Le plus souvent dans des conditions difficiles. Vivre comme mourir. Mais sans mot dire. Les mots d’ordre. Suivre les ordres… Alors, nous suivons les ordres. Nous n’interrogeons pas la parole qui vient d’en haut. Que faire ? Sans indications, sans mots d’ordre… Comment ferions-nous ? Que ferions-nous ? Des questions qui dérangent. Longtemps. Nous devrions être indépendants. Fixer nos propres règles. Vivre et mourir à notre façon, mais pour cela, il faudrait au préalable se poser des questions, voire pire… Chercher des réponses. Et cela ! Ah cela… Nous ne le voulons pas ! Jamais ! Alors, à la place, nous obéissons, et d’autres en profitent… \did{Silence.} Ils ont bien raison, puisque nous sommes amorphes, nous n’existons même pas… Enfin, à peine… L’espace d’un souffle, d’une durée déterminée de labeur, que la société nous impose, dans des conditions établies, bien à l’avance. \did{Silence.} Ils ont bien raison. Et ils ont bien raison de partir. Sans nous. Nous sommes un poids pour eux. \did{Silence.} Et eux, ils avancent. Ils ne savent pas où… Mais ils avancent ! Ils sont du côté du progrès ! Et nous, on les suit, on moutonne… On stagne.

\michael[piqué, réveil] Et comment feront-ils quand il faudra se salir les mains ? Hein ? C’est eux qui viendront faire le sale boulot ? Non…

\lingg Ah ! Ils feront ce qu’ils ont toujours fait. Ils réinventeront la domination, ils réorganiseront les hiérarchies actuelles en des hiérarchies nouvelles, se débarrasseront des inutiles, réinventeront quelques illusions le moment venu. Ils désigneront de nouvelles figures pour incarner l’insignifiance, la menace contre leur progrès… Et cela recommencera et recommencera. Sauf si… \did{Il fait un signe de tête vers la ville.} Mais ils ne s’en doutent même pas de ce qui arrive. \did{Silence.} Tout ne recommencera pas… Cette fois, tout semble bientôt terminé. \did{Il se tourne vers \michaelname.} Terminé, complètement, pour tout le monde ! Avec égalité !

\michael Vous croyez qu’ils veulent juste se débarrasser de nous ?

\lingg[ignorant la question] Le plus fantasque, c’est leur croyance en leur grandeur ! Comme vous, ils ont la foi. Ils croient en leurs projets. Sincèrement. \did{Silence.} Mais les flammes… Et ces questions si chères à notre société… Quelle utilité ? À ceci ou à cela ? À quoi cela sert-il ? Pour qui ? Pour quoi ? Le vide, il y aura du vide dans ces questions. \did{Silence.} Existera-t-il encore du sens lorsque nous ne serons plus là ? Avec nos corps aliénés pour attiser le feu qui s’éteint ? \did{Silence.} Qu’est-ce qu’une flamme si plus rien de vivant ne la nourrit ?

\oscar Les choses existaient avant nous, et les choses existeront après nous. \did{Il hésite.} C’est justement l’ordre des choses. Nos agitations ne résument pas la vie.

\lingg[enfoui, en lui-même enfoui] L’indifférence. Ou l’émotion. Vive. Et l’oubli. Et l’indifférence. Semblable. Cette tare qui ne perturbe même pas l’ordre des choses…

\michael[se tournant vers \oscarname] C’est pour cela que tu continues ? Pour ne pas perturber l’ordre des choses ? Ou simplement par lâcheté ?

\oscar Non, comme je l’ai dit… Par simple habitude.

\lingg[se tournant vers \michaelname] Vous savez son habitude, la répétition… Cela a quelque chose de rassurant ! Cela organise le monde, et cela vous empêche de vous rappeler.

\michael Des questions dont vous parliez ?

\lingg Non… Elles ne sont pas si perturbantes ces questions finalement. Elles dérangent. Un temps. Et on les oublie.

\michael Vous parlez de quoi alors ?

\lingg Je parle des souvenirs. De ces traces du passé qui vous harcèlent jusqu’à vous forcer à prendre conscience de ce que vous avez abandonné, et vous précipitent vers le plus terrible des soupçons, celui qui aiguillonne l’esprit, et vous hurle que vous auriez pu faire autrement, que cet ordre établi n’a rien d’immuable… Qu’il n’est pas le vôtre. Mais qu’il aurait pu l’être…

\michael[cafardeux] Je crois que je préfère ne pas me rappeler.

\oscar[travaillant] Moi, je me rappelle.

\begin{dida}
\michaelname et \linggname : vers \oscarname. Silence. \samuelname, absent, en son geste. Explosions.
\end{dida}

\lingg[curieux] De quoi vous souvenez-vous ?

\oscar Je me rappelle de tout. À quoi ça sert ? \did{Silence.} Comme vous le dites, tout sera bientôt terminé, pourquoi tenter de me tourmenter. Je travaille à la place. La plupart des gens y voient une corvée, pour moi, c’est un passe-temps. Je meuble. Comme vous et votre spectacle.

\lingg[captivé] Vous vous souvenez vraiment ?

\oscar Oui.

\lingg De tout ?

\oscar Je me souviens de tout.

\lingg[facétieux, un peu, entièrement, vers \oscarname] S’il vous plaît, racontez-moi !

\oscar[ennuyé] À quoi ça sert ? \did{Silence.} Bon… Si vous insistez… \did{Silence. \oscarname marche. Long. En large. Devant \linggname. Se fixe.} Je me souviens… \did{Silence.} Je me souviens de la guerre.

\lingg Fabuleux ! Racontez-moi !

\oscar[les mains dans les poches, regardant la ville] Je me souviens du silence. Des morts. De leur silence. Des visages qui ont disparu. Mais leur silence qui pèse lourdement. Sur ma mémoire. Et du silence, et des morts, et des visages.

\lingg Oui… \did{Silence. Souvenirs. S’en reviennent. Plus présents.} La mémoire, les machines, les machines avec des canons, et les morts avec leur fusil.

\oscar[terne] Oui… \did{Il signale : les déflagrations qui résonnent. La ville sous les résonances.} On s’habitue. La ville hurle. Brûle. On n’y prête pas attention. On ignore les morts. On tente. \did{Silence.} Je ne me souviens pas de leur nom. Je me souviens qu’ils étaient souvent des camarades. Je ne me souviens pas du nom des camarades. Je me souviens. On rigole, on hurle, on donne des ordres. On obéit. Pas toujours. Et puis, soudain, plus rien. Un bruit étouffé, le seul qu’on entend. Le bruit du fusil qui a tiré. Non. On ne l’entend pas. Ce bruit qui se noie parmi les autres détonations. Mais la balle qui atteint celui qui se tient à côté de vous. On l’entend. Elle résonne contre lui. C’est un bruit unique. Personnifié. Il n’existe que pour celui qui se tient à côté de vous. Ce bruit se souvient de son nom. Une oraison avant l’heure. Souvent la seule. Puisqu’on ne se souvient pas du nom des camarades. Puisqu’on ne se souvient pas du corps des camarades. On n’a pas le temps de revenir, de les enterrer. De se souvenir. Il faut survivre. Essayer. Alors. On collectionne ces sifflements. L’impact muet. Le corps qui tombe. À côté. Tout aussi muet. Et puis. Plus rien. On se dit qu’il va encore gueuler, rire, supplier, demander qu’on écrive à ses parents ou à son épouse. Rien. On le secoue. D’abord un peu. Puis beaucoup. Du silence. Juste ça. Et nous, on reste là. Bien vivants. Abrutis d’être encore vivants. Tout à côté des morts. Nous, on peut encore gueuler, rire, supplier, demander qu’on écrive à nos parents ou à notre épouse. Mais on ne dit rien. On commence à ne plus se souvenir. À ne plus avoir de nom. À quoi ça sert un nom tout à côté des morts. On est contaminés par leur silence. On ne peut qu’attendre. On attend. Cet éclat sourd et singulier. La balle qui nous est destinée. Et qui elle aussi attend bien patiemment. Qui se souvient de notre nom. Qui refuse encore de le dire. \did{Silence.} Cette balle, la mienne. Elle a trop attendu. D’autres ont reçu leur sauf-conduit avant moi. Ils s’en sont allés avec les autres. Tous taiseux. Moi là, parmi les déjà partis. J’ai attendu. J’ai essayé de me souvenir de leur nom. Pour rien. Des camarades qui ne disent plus rien. Qui pourrissent. Mais qui ne disent plus rien et qui pourrissent ensemble. Moi. Parmi eux. Seul. Et je ne me souviens plus de leur nom.

\lingg[affliction d’\oscarname, en lui] Oui… J’en ai connu beaucoup de ces corps. Silencieux. On les agite. Ils ne réagissent pas. On essaie de plaisanter avec eux. On ne les amuse plus. \did{Silence.} Pourquoi être planté là dans les gravats. À se battre pour des idées qui ne sont pas les nôtres. \did{Silence.} Si l’on reste seul avec le silence. Suffisamment longtemps. On devient le silence. Plus rien ne peut être nôtre. \did{Silence.} L’absurde. C’est toujours mieux à plusieurs. En chantant, en buvant, en crevant. \did{Silence.} Quand on attend la mort ensemble, personne ne devrait se débiner avant les autres.

\michael J’ai connu aussi. \did{Silence.} Je l’ai attendu aussi. Ce sifflement. Quelqu’un d’autre l’a reçu à ma place. Mais ce type à côté de moi. Ce n’était pas pour lui. Quand la chance passe. Une seule fois. \did{Silence.} Et la guerre se termine. Et puis, plus de sifflements. On doit continuer.

\lingg[détaché] C’est pour cela que vous voulez fuir ?

\michael Vous l’avez dit. On ne peut pas fuir. Ça ne sert à rien. \did{Silence.} Je veux juste partir d’ici. Je n’ai pas besoin d’oublier pour ça. Je veux juste… Un autre air.

\lingg[détaché, un peu plus] Un air qui n’est pas souillé par la mémoire.

\michael Juste un autre air.

\oscar[ignorant \michaelname, se tournant vers \linggname] C’est pour ça que je travaille. Pas pour oublier. Mais pour continuer à attendre cette balle.

\lingg[narquois, un peu] Et elle ne vient toujours pas ?

\oscar Non. Alors on commence à s’habituer. L’ordinaire, l’attente… \did{Silence.} J’étais plus impulsif avant, moins routinier. Ce n’est pas l’armée qui m’a donné ce goût de la routine. C’est la violence.

\lingg[narquois, moins] Et cette violence qui se répète. Qui ne change rien. Inconsciemment, on l’apprête. Elle recompose la vie jour après jour. La recrée. La même vie, mais recomposée. Sans s’en apercevoir. Greffée à la violence.

\oscar Moi, je ne la veux pas différente ma vie. \did{Silence.} J’aurais juste voulu qu’elle soit un peu plus bruyante.

\lingg Et après, beaucoup plus silencieuse.

\oscar Oui… \did{Il s’approche de \linggname.} Vous l’auriez voulu aussi ?

\lingg[d’une voix plus présente] Oui. Comme vous. Je l’ai tellement souhaité ce silence. Surtout après les attentats.

\oscar[se détournant] Pour vous, ces flammes, c’est le silence ?

\lingg Non… Non, non. Juste des souvenirs.

\oscar Ceux des attentats ?

\lingg Non.

\oscar Ceux de la guerre ?

\lingg Non.

\oscar Vous vous en souvenez bien de la guerre ?

\lingg Chaque jour. Cela ne me quitte pas.

\oscar On la fuit comme la peste, mais ça s’attache au-dedans. On ne peut pas. \did{Silence.} Et il ne reste plus qu’à attendre. \did{Silence.} Et à s’habituer à l’attente.

\lingg C’est la paix qui n’est pas naturelle. \did{Son bras, vers la ville, revient. Grâce.} L’ordre des choses… \did{Silence.} Constatez les flammes ! Encore aujourd’hui ! Après tout ce que nous avons vécu ! Et derrière les flammes, les morts. Et leur nom qui disparaît. \did{Silence.} Et l’on peut attendre, et d’autres attendront après nous… Mais cette fois… Peut-être que cette fois…

\begin{dida}
\samuelname soulève un ordinateur. Rangé dans la bibliothèque. Poussiéreux. Se prenant pour un livre. Il le porte à hauteur. De sa tête. L’examine. Examen. Un instant à la lumière. Et le laisse retomber. avec fracas. Les autres, stupéfaits. \samuelname tourne autour. Lentement. L’ordinateur, par terre. Plus vraiment d’allure, par terre. Soudain. \samuelname arrache son capot. L’éventre. Les autres, horrifiés. Il sort de ses entrailles des entrailles d’ordinateur. Puis un boîtier. À son tour. il le porte à hauteur. De sa tête. L’examine. Examen. Un instant à la lumière. Soudain. \samuelname se retourne. Brusquement. Les autres, embarrassés. \samuelname marche. Un pas lourd. Dans leur direction. Il se dresse au-dessus du fauteuil. Place sous le nez de \linggname le boîtier.
\end{dida}

\lingg[mal à l’aise] Est-ce aussi interdit ? \did{Pas de réponse.} Voulez-vous que je l’examine ?

\samuel[insensible] C’est à vous.

\lingg Sans doute, s’il était à l’intérieur de l’ordinateur… \did{Pas de réponse.} Je ne m’en sers plus vraiment. La plupart du temps, je reste assis là. \did{Pas de réponse.} Je ne sais pas ce que c’est.

\samuel C’est la mémoire.

\lingg Ah, très bien… Je crois que c’est à moi…

\samuel Ce n’est pas une question ! C’est à vous !

\lingg Je dois la prendre ?

\samuel Le règlement déclare que seule la mémoire sera remise à celui qui refuse le départ. Il s’agit d’une part de votre identité, et nul ne peut vous en priver. Personne ne doit attenter à la mémoire. Personne ! Même pas eux ! \did{Il secoue la mémoire pour faire signe de la prendre.} Prenez-la, c’est votre mémoire !

\lingg[interrogeant craintif \samuelname] Que dois-je en faire ?

\samuel Ce que vous souhaitez ! Tout être peut disposer librement de sa mémoire. Voilà ce que dit le règlement.

\lingg[saisissant la mémoire] Je comprends mieux. \did{Il observe la mémoire. Curieux. Curieusement.} La mémoire… Ce n’est donc que cela…

\samuel Oui. Vous pouvez l’employer comme bon vous semble. Il est possible de la recycler si vous le souhaitez.

\lingg[tendant l’objet à \samuelname] Je n’en ai pas besoin. Vous pouvez la prendre.

\samuel Vous êtes sûr ?

\lingg Oui, vous pouvez tout prendre.

\samuel Elle ne vous sera pas rendue par la suite.

\lingg J’en suis sûr, je n’en veux pas. Je n’ai pas besoin de mémoire. J’ai mes souvenirs. Cela me suffit. En attendant…

\samuel Très bien.

\begin{dida}
\samuelname reprend l’objet. Retourne finir l’examen. Jette la mémoire. Pas violemment. Pas vraiment. Mais violence quand même. La chute. Examine les restes. Examen. L’ordinateur, sans son allure d’ordinateur. Il trie. Ses outils pour le tri. Cette fois, méticuleusement. Composantes, à gauche, à droite. Du tri. Pose ses outils. Entrailles de la machine : la machine n’est pas son propre carcan. \oscarname et \michaelname l’observent. Calme recouvré. L’observent. Sans vouloir : se mouvoir. \linggname s’est tourné vers la ville. Sa ville parce qu’il insiste à ne pas la quitter. Regards. Tout lui semble parfait. Silence.
\end{dida}

\lingg[à \michaelname et \oscarname sans les regarder] Vous n’allez pas l’aider ?

\oscar[s’asseyant sur une caisse] Non, je crois que je commence à être fatigué. Je vais rester là, et le laisser faire.

\lingg[à \michaelname] Et vous ? \did{La pièce, entière. L’univers. Tout. Accable \michaelname. Accablements. Il ne répond pas.} Vous dites que vous avez fait la guerre, c’est exact ? \did{\michaelname ne répond pas.} Vous deviez être jeune. \did{\michaelname ne répond pas. \linggname indiquant \samuelname.} Votre ami semble plus jeune que vous. Lui il n’a pas dû la faire. \did{Silence.} C’est peut-être cela qui préserve sa gravité.

\oscar[montrant \michaelname] Il avait tout juste vingt ans. \did{Silence.} Ça vous brise une génération de mettre les mains dans les tripes des autres. \did{Silence.} Ça brise aussi les deux, trois générations qui suivent. Il faut du temps pour que les blessures cicatrisent. Et après, il faut du temps pour s’habituer aux cicatrices.

\lingg Et votre autre collègue ?

\oscar Il l’a faite aussi.

\lingg[curieux] Comment est-ce possible ? Il devait être adolescent. À peine.

\oscar Il a fait la guerre en perdant sa famille.

\lingg[se retournant pour voir \samuelname] Il est imperturbable. \did{Reprend sa posture. Distant.} C’est admirable.

\oscar La guerre, ça se réincarne en de multiples formes. Chacun a droit à la sienne. Pas besoin d’avoir vu le front. Elle vient à vous. S’adapte à la vie de chacun. Elle s’incruste. Malgré vous.

\lingg[froid, à lui, en lui] Nous étions plus âgés… Mais nous avions à perdre aussi… Étions-nous plus à même d’accepter cette perte ? Non. \did{Silence.} Non.

\oscar C’est pour cela qu’ils espèrent tant ce départ ! Beaucoup plus que nous ! \did{Zieute \linggname.} Je veux dire que moi en tout cas. Vous êtes déjà à un stade plus avancé du renoncement.

\lingg[regardant \oscarname] Oh… Juste une habitude… Je suis comme vous, mais je me suis habitué à être comme vous.

\michael[à demi-voix] Moi aussi…

\oscar[ne prêtant pas attention] C’est peut-être la cause de ces incendies. La frustration après tant de sacrifices, l’impossibilité de s’habituer.

\michael Ils m’ont rejeté. Moi aussi ! \did{\samuelname se redresse. Son visage marque une émotion. Semble marquer. Du chagrin. Non. Juste une surprise. \michaelname se tourne. Vers \oscarname. Surprise.} Je l’ai seulement appris… Avec les émeutes… \did{Silence.} Je m’en doutais depuis un moment. Je ne sais pas pourquoi. \did{Silence.} Je me disais que c’était trop beau pour moi, que je ne le méritais pas. \did{Silence. \michaelname montre la ville.} Et puis comme les autres… J’ai appris ça ces derniers jours… Avec le durcissement des conditions de départ… \did{Silence.} Ils disent que je ne suis pas suffisamment utile, que mon potentiel est commun. Qu’au nom de la raison commune… Mon savoir, commun… Mes capacités, communes… Qu’est-ce que ça veut dire commun ? \did{Silence. \samuelname reprend. Mécaniquement son tri. \michaelname ne se détourne pas de la ville.} En somme, je suis insignifiant. Commun, comme eux. \did{Silence.} Peut-être est-ce pire encore. Je ne suis même pas capable de participer. D’aller nourrir leur fureur. \did{Silence.} Leurs émeutes ne m’intéressent pas.

\oscar[s’approchant de \michaelname] Ce n’est pas possible… Encore moi, je peux comprendre. Je suis vieux. Mais toi… Tu as l’espoir, la vitalité. Tu mets tellement d’énergie dans ton travail. Tu dois te tromper. Tu as cru voir ton nom sur la liste…

\michael Il n’y a pas d’erreur. J’ai aussi reçu un message personnel. \did{Silence.} On m’a informé personnellement. \did{Silence.} C’est la première fois qu’ils m’écrivent… Personnellement.

\oscar[morne] J’ai aussi reçu cette lettre, avec mon nom dessus.

\michael[amer, rires] Elle n’était même pas personnelle cette lettre alors. \did{Silence. Amer, sans rires.} Pourtant, je me suis tellement battu. \did{\michaelname saisit \oscarname. Mortifications.} J’ai tellement cru en eux !

\oscar Je sais.

\michael Tous les jours, je leur donne ma vie ! Pour leur cause !

\oscar Je sais.

\michael Je la partage même, leur cause ! Je crois en leur raison ! Commune ! Je lutte avec conviction pour elle. Chaque jour ! C’est ma vie que je leur donne !

\oscar Je sais.

\begin{dida}
\michaelname lâche. Marche. Tombe. À peu près. Se lève. Marche encore. Furieusement, un peu. Il s’arrête. Observe \oscarname et \linggname. Tente de dire quelque chose. Ne dit rien. Il reprend sa marche. S’arrête à nouveau. Tente de dire.
\end{dida}

\michael[tendant son bras, montrant son bras, hurlant] Regardez ! \did{\oscarname et \linggname observent attentivement. Le bras est un bras. Tout ce qu’il y a de plus normal. Pour un bras.} Vous ne voyez rien ? \did{Aucune réponse.} Moi, je ne ressens rien ! \did{Il frappe son bras. Avec violence.} Rien !

\lingg[indécis] Que voulez-vous dire ?

\michael[tombant, à genoux, à peu près] Je ne ressens plus rien… \did{Silence. Sursaut de nervosité.} Et je dois payer pour ça. Je dois continuer à rembourser tous les jours. C’est à crédit que j’ai droit à un bras !

\oscar[s’approchant, flottant] La guerre ?

\michael[replié sur lui-même] Oui. À la fin de la guerre. Une explosion, je suis sonné. Je reviens à moi. Peu à peu. Je retrouve mes esprits. Mais je ne retrouve plus mon bras. \did{Il saisit son bras. Le frappe.} Du vide, juste du vide ! Je cherche autour de moi. Mais rien. Rien partout ! Du vide ! Partout ! Alors je cherche. Je continue. Nerveusement. De plus en plus. Mais ça ne sert à rien. Disparu ! Plus de bras ! Rien ! Du vide ! \did{Silence. Se calmant.} Et puis. Je ne me souviens plus de rien. Du vide. J’ai dû m’évanouir. Je me suis réveillé dans un hôpital. Ils m’avaient mis une prothèse. Sans me demander. Un de ces machins militaires. Sans confort. Impossible à utiliser normalement. À chaque mouvement. La douleur. Le rappel de ta blessure. Beaucoup plus que s’il n’y avait pas de prothèse. Juste du vide.

\lingg[soucieux] Pourtant, il semble bien réel… Enfin humain…

\michael Oui, mais ça, c’est après. Après la guerre. Ils sont venus me voir. J’étais au chômage. Je vivotais. Avec les quelques aides que je recevais. Eux… Ils ne m’ont pas jugé comme un invalide ou un assassin. Parce que c’est ce que nous sommes ! Soit des invalides. Soit des assassins. Ou alors des morts. On ne se bat pas pour des idées. On obéit. On tue. On bousille des inconnus. \did{Silence.} Ils sont venus me proposer de me mettre une prothèse construite à partir de mes cellules. \did{Silence. Regardant son bras.} J’ai accepté.

\oscar Tu n’as pas pu te payer une telle prothèse avec des aides sociales…

\michael Ils m’ont justement fait une offre. Pour service rendu qu’ils disaient… Un rabais et un remboursement échelonné sur le long terme. En plus, ils me proposaient un travail. Alors qu’il n’y en avait pas du travail pour un type comme moi. J’ai accepté. J’ai adhéré à leurs valeurs aussi. Et puis aujourd’hui on me laisse tomber. \did{Silence.} Ils vont sûrement venir le reprendre. Comme je suis devenu inutile.

\oscar Mais non, ils ne peuvent pas faire ça !

\lingg Pourquoi ne le ferait-il pas ?

\oscar[choqué, un peu] Je ne sais pas… \did{Il réfléchit. Tente de refuser. Cherche.} Et c’est trop coûteux de le reprendre…

\lingg Peut-être bien… \did{A \michaelname. Rassurant.} Tout sera bientôt terminé. Pour eux aussi. Il ne restera rien. \did{Silence.} Et c’est trop coûteux…

\michael[à son bras] Si vous me piquez, j’ai mal. Mais je ne ressens rien. Ce sont mes cellules. Elles sont produites à partir de mes cellules. Ce sont donc mes cellules. Mais ce ne sont pas mes cellules. \did{A \linggname.} Vous comprenez ?

\lingg Je comprends. Ce n’est pas votre bras. C’est une dette.

\michael[plus calmement] J’étais volontaire. De la première heure ! Et puis mutilé, jute avant la fin ! \did{Silence. Ricanements. Plus de ricanements.} Pas de chance… La plupart de mes amis sont morts. Et les seuls qui en ont profité réellement de tous ces macchabées, c’est eux ! \did{Silence.} Je me disais qu’ils étaient reconnaissants. Pour la guerre, le sacrifice. Et qu’ils voulaient nous rendre justice. Je pensais qu’on travaillait ensemble. À un renouveau. Une égalité pour tous. \did{Il se tourne vers la ville.} Mais, une fois qu’on n’a plus besoin de nous… Nous voilà seuls. Encore. Nous sommes notre mensonge.

\lingg Oui, nous sommes votre mensonge.

\michael[à lui-même] Quelle justice ? \did{Silence. À \linggname.} Je n’ai pas de colère. Rien en moi. \did{Il fait un signe de tête vers les incendies.} Je ne suis pas capable de faire ce qu’ils font. Même si, comme eux, j’ai été trahi.

\lingg Ces flammes n’ont rien à voir avec de la trahison. Elles sont indépendantes de toute volonté. Elles ont leurs propres règles.

\michael[ignorant \linggname, se détournant de la ville] Après tout, peut-être qu’ils ont leur raison. De nous abandonner ici.

\lingg Vous leur cherchez des excuses. Tout le monde a ses raisons. Diverses. Variées. C’est pour cela qu’il n’y a pas de raison. Commune ou non. Aucune raison. Nulle part. Des instincts, tout au plus.

\oscar[entourant de son bras \michaelname] Ce n’est pas grave. On restera ici ensemble. On trouvera bien quelque chose à faire.

\michael Tu ne crois pas ce que tu dis.

\oscar Comment a-t-on fait pendant tout ce temps ?

\michael Il n’y a plus d’avenir. Ici. Maintenant. Tout brûle.

\begin{droite}
Voltent. Observent le spectacle. Silence.
\end{droite}

\oscar[faisant un geste vers \linggname] Peut-être a-t-il raison… \did{Il s’assied péniblement sur une caisse.} C’est bientôt terminé. Pour eux aussi. Pour tout le monde, comme il le dit…

\michael[errant, dans sa voix, dans ses pas] Qu’il ait raison, qu’il n’ait pas raison, qu’ils aient raison, qu’ils n’aient pas raison. Nous avons raison, c’est moi qui ai raison, c’est toi… \did{Silence.} Cela ne change rien si tout se termine. Il n’y a pas d’avenir ici. Alors, qu’il n’y ait pas d’avenir pour tout le monde, qu’il y en ait pour certains… À la fin… Si on abandonne une partie d’entre nous… C’est comme si on abandonnait tout le monde… Non ? Comment continuer sans eux… Comment continuer sans nous… \did{Il se place devant \oscarname.} Le mensonge… Il est dans nos têtes, dans notre obéissance, et… \did{Silence. Se détourne.} Ce qu’ils devraient chercher à savoir, c’est comment ils vont faire pour continuer sans nous…

\lingg[indifférent] Ils vous remplaceront, l’esclavage, cela se renouvelle de maintes façons. Ils ont dépassé l’état dans lequel vous pataugez. Ils ne se posent pas de questions. S’ils en ressentent le besoin, ils vous emploient. S’ils en ressentent le besoin, ils sont affables. S’ils en ressentent le besoin, ils vous brisent.

\michael[agacé] Alors quoi ? On attend, c’est tout ?

\lingg[inchangé] Oui, on attend. \did{Silence.} On profite du spectacle, en attendant.

\michael En attendant quoi ?

\lingg[traînard] Vous verrez bien ! Attendez ! Un peu encore. Juste un peu ! Que tout s’enfonce au-dedans de la terre. \did{\michaelname par terre. Se pose. À côté du fauteuil. Il regarde la terre.} Faites comme moi ! Contemplez ! Le feu ! Recueillez-vous en lui ! Il vous déchargera de votre détresse. Et si vous comprenez qu’il n’a pas de raison, lui, qu’il est là, toujours, comme une cause première… Beau, inéluctable… Il vous murmurera une promesse. \did{Silence.} Tout sera bientôt terminé.

\begin{dida}
\samuelname se relève. Outil à la main. Un marteau. S’approche. Il se tient sévère devant \oscarname. Qui demeure proscrit. Il déambule prestement. La pièce se déforme. Le visage se déforme. Préoccupé. Non. Il passe plusieurs fois devant \linggname et \michaelname. Qui ne le voient pas. Il est devenu invisible. L’ignore la pièce. L’ignorent les autres. Il se plante face à \linggname. Face à \michaelname. S’apprête à parler. Marteau solidement attaché à sa main. Comme si les mots allaient venir du marteau. Ne parle pas. Marteau non plus. Pourtant son corps refuse le silence. Silence.
\end{dida}

\samuel[austère] Non ! \did{Tous l’ignorent. Il ne hurle pas.} J’ai dit non ! \did{Tous l’ignorent. Calme. Sa sévérité. Ne hurle pas.} Vous êtes devenus des lâches. \did{Tous lèvent la tête.} Après les flammes, vous vous réjouirez des cendres. Vos communierez avec elles. Jusqu’à devenir vous-mêmes cendres. Vous vous trompez. Après les cendres, il n’y aura plus de flammes. Il n’y aura plus la chaleur des flammes. Seulement le froid. Encore le froid. Immuable. L’espace froid. Vide. Désespérément. Le monde s’enflamme. Rarement. Mais cette fois, il s’enflamme avec vous, et vous vous enflammerez avec lui. Et après le feu, les cendres, et après les cendres, le silence. Et si ce silence nous envahit, jamais il ne disparaîtra.

\lingg[haussant les épaules] C’est peut-être ce que nous souhaitons \did{\samuelname s’éloigne. Retour à son travail.} Les cendres, le froid, le silence. Dans les flammes. Juste cette promesse. \did{\samuelname l’ignore. À son travail.} Juste profiter de l’embrasement. De sa rareté. Et pourquoi s’occuper de demain ? La beauté, là ! Incompréhensible ! Face à nous ! Et elle passe. \did{Silence.} Maintenant, face à nous ! \did{Silence.} Vous devriez profiter du spectacle. Après… Ce sera trop tard. \did{À \michaelname.} Leur ailleurs… Il n’y a pas d’ailleurs dans ce qui est maintenant. Juste là ! Devant nous ! Pourquoi devrions-nous avoir un avenir ? \did{Silence.} Nous n’avons pas de vision. \did{Silence.} Ce maintenant, nous ne le voyons même pas. \did{Silence.} Profitons du spectacle, pendant qu’il en est encore temps.

\michael[déception, un peu] Nous aurions pu avoir une vision là-bas. Ensemble. \did{Geste de la tête vers \samuelname.} Il a raison, on aurait pu tenter de maîtriser ce feu. Maintenant, on doit contempler… On ne peut plus participer.

\lingg[signe en direction de la ville] Vous pouvez rejoindre vos camarades ! Vous pouvez encore participer ! \did{Silence.} Il y a parfois des choses plus grandes que soi… Allez faire feu sur le monde ! Participez aux flammes !

\michael[renfrogné] Non… Ça ne sert à rien… Quelques étincelles… Un écran de fumée pour soi-même… On ne peut pas revenir… Notre volonté… Et j’ai assez tué…

\begin{dida}
\michaelname lève les yeux. Au ciel. Ailleurs. Vers ce qui bannit. Parmi les flammes. il fait un geste. Frappe la terre. Se lève. Marche. Se rassoit. Pas de colère. À rien. Il ne croit pas. Pas de colère. Silence.
\end{dida}

\oscar[se ranimant, un peu, à \samuelname] Et toi ? \did{\samuelname continue. L’ignore. Son travail pour l’ignorer.} Tu continues alors qu’on n’y croit plus… Tu continueras après nous, sans doute… \did{\samuelname continue.} Tu ne dis rien… Je te comprends. Je faisais comme toi avant. La tête droite, le regard clair. Pas de lamentations tout autour. On impose le silence. Du travail. Rien de plus. S’esquinter pour ne pas… \did{Un geste montrant \linggname et \michaelname.} Regarde-nous ! Faut pas… \did{\samuelname continue.} Tu ne dis rien… Mais on va te manquer quand tu partiras. Peut-être que non, en fait… Tu oublieras… Oui, tu continueras après nous, sans doute… À notre place ! Et tu ne te souviendras plus de nos noms. Progressivement. Pas tout de suite après, mais progressivement… Plus de souvenirs. C’est que tu ne t’arrêtes même pas pour essayer de les retenir. C’est mieux. Pas besoin de les préserver. \did{\samuelname continue.} L’absence de tes souvenirs… Ce sera notre paix ! Nous pourrons réellement disparaître. \did{Silence.} Plus de noms. Même après. Juste disparaître. Un peu d’existence après les cendres… Non… Plus de noms. Plus rien.

\samuel[posant ses outils, se redressant] Je travaille. Je ne me pose pas de questions. J’accepte ce que vous refusez.

\michael On refuse quoi ?

\samuel L’ennui. Je ne travaille pas si efficacement parce que j’ai un quelconque don. Ou une quelconque foi. A-t-on réellement besoin d’habileté pour faire ce que nous faisons ? Il faut juste savoir courber l’échine. Oublier que l’on courbe l’échine. Je travaille ainsi parce que j’ai accepté l’ennui. Chaque geste me rapproche un peu plus de l’ennui. Faire corps avec l’ennui. Une carapace contre le réel. Le réel. La répétition m’y soustrait. Sans tourments. Continuer. On travaille. On continue. Jusqu’à l’ennui. Et là, l’échappatoire. On peut se précipiter. \did{Silence. S’approche d’eux. Calme.} Vous rêvez. Ils sont doux vos rêves. Vous prenez le risque. Celui de la chute. Et quand vous vous y confrontez un peu trop à vos rêves. \did{Il fait révolution. Sur lui-même.} Cette réalité. Vous la subissez plus durement. Sans vos rêves. Et la douceur de vos rêves. Elle s’effrite. Cette réalité vous retire tout. Bien plus que vos simples rêves. Votre confort aussi. Celui dans lequel vous vous étiez blottis pour rêver. Mais si on travaille. Vide. Sans espoir. On devient cette réalité. Elle ne peut plus nous devancer.

\michael Je ne comprends pas… Comment… Sans espoir…

\begin{droite}
Silence. Explosions. Des silences.
\end{droite}

\lingg Vous aussi. \did{Silence. \samuelname reprend son travail.} Nous serons plus nombreux que prévu. À nourrir le feu.

\oscar[angoissé] Ils ne veulent pas de toi non plus ?

\samuel Pourquoi voudrais-je partir ? Pire. Espérer. À plusieurs. C’est répugnant. L’espoir. À plusieurs.

\oscar Attends ! C’est toi qui as refusé ton départ ?

\samuel Non.

\michael[à \oscarname] Pourquoi dis-tu qu’ils ne veulent pas de lui non plus ? \did{À \samuelname.} Pourquoi est-ce qu’ils ne veulent pas de toi ?

\samuel Ils ne veulent de personne. \did{Continue. Son ouvrage. Son geste se répète. Parfait. L’ennui.} Vous ne l’aviez pas compris ? Ils ne veulent pas de vous, ils ne veulent pas de moi. \did{Tend son bras vers \linggname.} Ils ne veulent même pas de lui.

\oscar Mais on est là pour sa liquidation ?

\samuel On est là pour faire le tri de ses biens ! Pour servir leurs objectifs ! En quoi est-ce que ça lui profite ? \did{Silence. Il travaille.} Quand ils n’auront plus l’utilité de sa personne… Ils ne se préoccuperont plus de lui. Peut-être savent-ils déjà qu’il ne veut pas partir, et ça doit les arranger. Ils ne se préoccupent de personne.

\lingg[éthéré] Ils ne l’avaient pas compris. Nous sommes leur mensonge.

\samuel Oh non ! Il n’y a pas de mensonge. Ils n’ont rien promis. C’est vous qui avez cru en ce qu’ils ne disaient pas. L’erreur vient de vous. Pas d’eux. Ils se sont servis de votre foi. Rien de plus. Rien de moins. Peut-on réellement les blâmer ? Ils seraient responsables de votre crédulité ? Vous n’êtes qu’un prétexte. \did{En direction de \linggname.} Même votre croyance dans ce feu. Rien qu’un prétexte.

\oscar[hagard, s’approchant de \samuelname] Que t’ont-ils dit ? \did{Silence. Pas de réponse.} C’était quoi le prétexte… Pour ton cas ?

\samuel Ça fait bien longtemps que je le sais. Dès le début. Je ne voulais pas partir de toute façon. Je voulais juste travailler. Oublier. L’ennui. Et ça leur convenait. Ils m’ont donc laissé travailler. \did{Silence.} Et ça me convenait aussi. \did{Il travaille. Travail.} Pas de raison commune. Mais un commun accord.

\michael[affolé, un peu] Mais ils ne peuvent pas tous nous laisser ici. Ils ont besoin de nous ! \did{Silence. Étouffe. Commence à s’effondrer en lui-même, un peu.} Ils t’ont donné quoi comme raison ? \did{Disparaît de plus en plus en lui-même.} Tu as su ça quand exactement ?

\samuel Après la guerre. Pendant la période des attentats. \did{Se tourne, le visage amusé.} Je suis considéré comme un inadapté politique.

\michael Ça veut dire quoi ?

\samuel Je suis une menace. \did{Il constate l’expression d’incompréhension de \michaelname.} Je suis un vecteur de discorde au sein d’un groupe. Un sabot dans les rouages. \did{Silence. Il se tourne vers \michaelname.} Je suis un potentiel risque pour ce qu’ils appellent départ.

\oscar À cause de quoi ? Qu’est-ce que tu as fait ?

\samuel[montrant les livres] Ces trucs… Après la guerre, je m’y suis mis… \did{Il fait signe vers \linggname.} Pour lui, ils ont considéré ça comme une plus-value. Pour moi, c’était un danger. \did{Silence.} Je n’avais pas d’argent. Ils m’ont permis de travailler. Comme ça, ils m’avaient à l’œil. \did{Silence. Travaille. Arrêt.} Eux non plus. Ils ne comprennent pas. L’ennui. Ce que je souhaite. \did{Silence.} Et moi, je travaille. Il n’y a rien à ajouter. Et je me suis tenu tranquille, alors… J’ai pu continuer à travailler.

\michael[s’asseyant] Ils ne peuvent pas…

\samuel Je ne veux rien… J’ai tout ce qu’il me faut… Pas besoin de croire en leur départ. Je ne suis pas là pour ça.

\lingg[curieux] Pourquoi êtes-vous là alors ? Uniquement pour l’ennui ? Pourquoi continuer à travailler pour eux ? Vous pourriez faire autre chose ? Pour vous ennuyer…

\samuel A-t-on réellement le choix ? Il n’y a plus de travail à part celui qu’ils fournissent.

\lingg C’est vrai. \did{Silence.} Vous pourriez aussi travailler contre eux. Faire feu !

\samuel Je suis là pour l’ennui. \did{Silence. Travaille. Arrêt.} L’ennui se trouve là. Dans mon travail. Pas dans ces flammes. Ces illusions. Ces petits rêves égoïstes. Je ne veux pas faire feu. Je veux faire ennui. \did{Silence. Travaille. Sans arrêt.} Simplement. Rien d’autre. Travailler. Encore et encore. Et devenir l’ennui.

\michael Si nous ne partons pas… Qui part ? \did{Silence. Il se tourne vers \linggname} Qui ?

\oscar Je ne sais pas… Eux, dans tous les cas. Ils nous ont utilisés pour ça… \did{Silence. Dirige son regard. Vers la ville.} Pas eux, par contre. Eux et leur déception. Et maintenant leur colère. \did{Silence.} Cela ne leur sera pas d’une grande utilité.

\michael[fiévreux] Peut-être qu’ils pourront les empêcher de partir ! Les obliger à les prendre avec eux ! \did{Se levant. Fiévreux, plus.} Oui, les obliger ! Avec la force ! Ou alors !

\lingg Le feu !

\oscar J’en doute… Ils ont dû y penser avant de les avertir. Ils ont dû prendre leurs dispositions. Se protéger.

\michael[retombant] Oui, ils ont dû faire en sorte de protéger leur départ. Sans nous. \did{Silence.} Nous avons tellement… Obéi. Travaillé. Dans quel but ?

\oscar[engourdi] Dans le leur… Et nous avons réalisé ce qu’ils souhaitaient. \did{Silence. Il se tourne vers \samuelname. Il l’observe.} Peut-être a-t-il raison. Lui aussi.

\michael[criant, déraison, un peu] Qui a raison ? Tout le monde a raison, personne n’a raison… \did{Silence. Observe son bras. Déraison, un peu plus.} Et moi… S’il n’y a plus rien… Même plus de raison…

\oscar[le coupant] Ils ont tous raison. Ceux qui veulent partir, ceux qui veulent brûler la ville, ceux qui veulent contempler les flammes, ceux qui veulent l’ennui, ceux qui veulent trouver une raison. On grouille avant la fin. Cette fin n’a de raison que parce qu’elle est notre fin. À tous ! Et il faut attendre…

\lingg Pour les flammes, il n’y a pas de raison. Nous sommes un simple aléa.

\michael Je ne peux pas attendre. Sans raison. C’est insupportable !

\oscar Nous avons travaillé pour l’ennui. Pour le feu aussi. Pour la fin. Pour cet instant qui arrive, où tout s’arrête. Soudainement. Une raison ? Non. Nous sommes des bêtes. La raison, elle nous est impossible. \did{Silence.} Nous ne le savions pas. Mais nous ne voulions pas… \did{Silence. À \michaelname.} Grâce à nous, tout fut construit. \did{Silence. À la ville.} Et tout s’anéantit maintenant. Grâce à nous !

\michael[le visage entre ses mains] Pourquoi ? \did{Silence.} Pourquoi nous, on ne pourrait pas profiter aussi ? Que vont-ils faire sans nous ? Je ne peux pas imaginer qu’ils vivent sans moi ! \did{Il se précipite sur \oscarname.} Ils ont besoin de moi, ils ont besoin de mon travail ! Regarde, il n’y a rien ici ! Juste cette ville qui brûle ! Moi, je ne veux pas rester là ! Je ne veux pas être cette ville qui brûle !

\oscar[le saisissant par les épaules, compréhensif] Je sais. \did{\michaelname le lâche. Vacille dément.} Tu te trompes. \did{Silence.} Eux aussi. Ils ne partiront pas. \did{Silence. Il montre la ville.} Eux aussi. \did{Silence. Il montre \samuelname et \linggname.} Eux aussi. \did{Silence.} Tout s’écroule pour tout le monde. Leur vanité… Penser y échapper, pouvoir fuir. Cela y participe. Tout comme notre docilité. \did{Vers les incendies.} Et maintenant, notre rage. Tout cela fait sens. \did{Silence.} C’est merveilleusement beau. Absurde. Les lois. Inexorables. L’ordre des choses. Tout s’enflamme. Les cendres. Tout devient autre. \did{Silence.} Absurde. Mais absolument beau. \did{Silence. S’enflamme, un peu.} Même dans la destruction. Nous restons ouvriers. Elle existe. La destruction. Réelle. Grâce à nous. Et jamais grâce à eux ! Grâce à nous : la destruction ! \did{Silence. S’éteint, un peu.} Grâce à nous… La destruction…

\michael Je fais quoi moi ? Maintenant ? Je dois choisir quoi ? La révolte, la fatalité ? Le suicide ? \did{Furieux.} L’ennui ? Les flammes ?

\oscar[sépulcral] Peu importe…

\michael[perdant pied] Que faire ? la ville brûle… \did{Silence. Il s’agite. \samuelname relève la tête. L’observe. Observations. Reprend son travail. Travaille. Il est le travail. Ou l’ennui.} Il n’y a pas d’avenir. Ici. Maintenant. Tout brûle.

\lingg[serein] Ne les écoutez pas ! \did{\linggname ouvre les bras vers la ville. La brûlure.} Ressentez sa puissance ! \did{Silence.} Eux, ils ne la comprendront jamais cette beauté. Une ville en feu. Ils ne comprennent pas le feu. Ils sont attirés par des lumières au loin. Mais celle-là de lumière ! Qui veut résonner en eux ! Ils la bafouent. Cette ville en feu. Parfaite. Avant la fin. Qui devient la cause première. L’ordre des choses. Ils ne comprennent pas le feu. Ils seront emportés par lui. Nous aussi. Tout le sera. \did{Silence.} Mais vous pouvez encore le comprendre. Le contempler. Activement. Jusqu’à vous unir à lui. Devenir le lien. Qui unit chaque entité qui existe. Jusqu’à sa destruction. Jusqu’à votre destruction. \did{Silence.} Ils veulent résister. Qu’ils résistent. Ils croient pouvoir le maîtriser. Qu’ils essaient. Mais ils ne comprennent pas sa nature. Ils le dénigrent. Ils le craignent. Ils le fuient. Mais partout. En toute chose. Il est là. Il sommeille. Il se meut. Il fait vivre. Il laisse vivre. Il donne. Et puis. Il reprend. \did{Silence.} Votre avenir. Le feu. Ce qu’il devient. \did{Du même ton qu’\oscarname. Pareil.} Beau. Peut-être bien. Implacable. La ville s’enflamme. Les cendres. Nos souvenirs. Tout devient autre.

\michael[refusant, à lui-même, aux cendres] Que nous reste-t-il ?

\lingg Il nous reste la ville qui brûle. \did{Silence.} Encore un peu. \did{Silence.} Encore un peu.

\curtain
