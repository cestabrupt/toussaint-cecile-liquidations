# ~/ABRÜPT/CÉCILE TOUSSAINT/LIQUIDATIONS/*

La [page de ce livre](https://abrupt.ch/cecile-toussaint/liquidations/) sur le réseau.

## Sur le livre

L’humanité se liquide sans peine lorsque ses structures s’enflamment. Cette pièce de théâtre se place à une frontière : celle de la parole dite, de la poésie lue, de l’écriture historique de notre futur. Elle parle de l’effondrement, et du sens que l’on cherche désespérément lorsque le spectacle ne miroite plus que le silence.

## Sur l'autrice

Née à la fin des années 1980 à Marseille. D’origine haïtienne. Un père encadreur, une mère sculptrice, ayant fui tous deux la dictature. Une pratique structurante du violon, le dessin et la littérature comme accompagnements. Elle fera du premier son métier. Influencée par le travail du bois de ses parents, elle décide de devenir luthière. Après une formation à Florence, elle s’installe à Gênes. Sa littérature cherche un dialogue avec l’étrange, le lien qui s’est brisé entre l’humain et sa nature.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
